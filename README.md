<h1 style="text-align:center">
UJM_L2_S3 PROGRAMMATION_IMPERATIVE_I
</h1>

## Description

création d'un jeu en C avec la bibliothèque MLV (construite au dessus de la bibli SDL)

## Astuce: comment jouer ?

Dans le menu du jeu si vouc cliquez sur le logo bomberman
les règles du jeu et les commandes s'affichent.

<details>
<summary style="font-weight:bold;font-size:large">Aperçus du jeu</summary>

![page d'accueil du site](/doc/screen-shots/home.png)

![règles du jeu](/doc/screen-shots/rules.png)

![lancement du jeu](/doc/screen-shots/start.png)

![jeu en cours](/doc/screen-shots/game.png)

![animation de l'explosion d'une bombe](/doc/screen-shots/explosion.png)

![fin du jeu](/doc/screen-shots/end.png)
</details>

## Structure du projet

<style>
tr:nth-child(even) {
  background-color: gray;
}
</style>
<table>
    <thead>
        <tr>
            <th align="right">Nom module</th>
            <th align="center">Fonctionnalités</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>bomberman</td>
            <td>
            <div align="center">
            contient la fonction main qui fait l'affichage du menu du jeu :
            </div>
            <ul>
                <li>(logo) BOMBERMAN -> si on click dessus les règles du jeu s'affichent.</li>
                <li>Nouvelle partie solo</li>
                <li>charger une partie</li>
                <li>multi-joueur local</li>
                <li>tableau des légendes</li>
            </td>
        </tr>
        <tr>
            <td>règles.h</td>
            <td>affiche les règles du jeu.(~pendant 10s)</td>
        </tr>
        <tr>
            <td>type</td>
            <td>
            contient tous les types nécessaires au jeu :
                <ul>
                    <li>type map</li>
                    <li>type joueur</li>
                    <li>type ennemi</li>
                    <li>type obstacle</li>
                    <li>type booster</li>
                    <li>type bombe</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>affichage.h</td>
            <td>
            contient les images, animations, lecteurs d'animation.
            </td>
        </tr>
        <tr>
            <td>jeu</td>
            <td>
            contient la fonction a exécuter si partie solo est sélectionée.
            </td>
        </tr>
         <tr>
            <td>joueur</td>
            <td>
            contient toutes les fonction consernant les actions du joueur.
            </td>
        </tr>
        <tr>
            <td>ennemis</td>
            <td>
            Contient toutes les fonction consernant les actions des ennemis.
            </td>
        </tr>
        <tr>
            <td>pause</td>
            <td>
            Contient la fonction permettant de mettre le jeu en pause.
            </td>
        </tr>
        <tr>
            <td>sauvegarde</td>
            <td>
            Contient les fonctions permettant de sauvegarder/charger une partie. Contient aussi le type configuration.
            </td>
        </tr>
        <tr>
            <td>jeu_local</td>
            <td>Fonction pour lancer une partie locale..</td>
        </tr>
        <tr>
            <td>meilleurs_scores</td>
            <td>
            Contient les fonctions permettant d'enregister/d'afficher les 10 meilleurs scores.
            </td>
        </tr>
    </tbody>
</table>

## Lancer le projet

Pour créer l'exécutable tapez: make
Pour supprimer les .o et l'exécutable tapez: make clean

pour lancer le jeu taper: ./bomberman

## Build

cloner le répot et utiliser docker ou installer les dépots 
manuellement.

Pour créer l'exécutable tapez: make
Pour supprimer les .o et l'exécutable tapez: make clean

pour lancer le jeu taper: ./bomberman

### next update

put docker image in tar format and in docker hub to ease the
test of the project (fixing some dependency bugs first)