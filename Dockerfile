# ------------ #
#    BUILD     #
# ------------ #
FROM ubuntu:20.04
#AS build

# update and install Linux packages
RUN apt-get update && \
    apt-get install -y \
    gcc \
    make \
    build-essential

# copy source code and dedpendencies packages
COPY ./bomberman /app/bomberman

# install mlv library :
# 1. installation of dependdencies
ENV TZ=Europe/Paris
RUN DEBIAN_FRONTEND=noninteractiv \
    apt-get install -y libsdl-gfx1.2-dev libsdl1.2-dev libsdl-mixer1.2-dev && \ 
    apt-get install -y libsdl-ttf2.0-dev libglib2.0-dev libxml2-dev && \
    apt-get install -y libsdl-image1.2-dev

# 3. install mlv packages
RUN dpkg -i /app/bomberman/lib/mlv/libmlv0_2.0.2-1_amd64.deb \
    /app/bomberman/lib/mlv/libmlv0-dev_2.0.2-1_amd64.deb

# app compilation in binary code
COPY ./makefile /app
WORKDIR /app
RUN make

# launch the app
WORKDIR /
CMD ["/app/bomberman/bin/bomberman"]

#After some research it seems that docker need x11 forwarding
# in order to open graphical interface.
# I'm on it !


# not implemented yet, problem with the location of installed library
# that need to be transfer in the prod container.
# For now I try the '-static' option of gcc in order to include 
# librarys into the excecutable but wuthout success.
# ------------ #
#    PROD      #
# ------------ #
# FROM ubuntu:20.04 AS prod

# # prepare the container to run the app
# RUN adduser --disabled-login player &&\
#     mkdir -p /app/save

# # get the app frorm the build stage
# COPY --from=build /app/bomberman/bin/bomberman /app/bomberman
# RUN chmod +x /app/bomberman

# # change the current user
# #USER player

# # launch the app
# CMD ["/app/bomberman"]