/*
Fonctions permmettant de gerer l'affichage du jeu bomberman.
*/
#ifndef _AFFICHAGE_H_
#define _AFFICHAGE_H_

/*______fonction d'affichage de nos types______*/
void effacer(int x, int y);
void afficher_obstacle(int x, int y);
void afficher_joueur(int x, int y);
void afficher_joueur_up(int x, int y);
void afficher_joueur_left(int x, int y);
void afficher_joueur_right(int x, int y);
void afficher_bombe(int etat, bombe *bombe);
void afficher_ennemi(int sens, int x, int y);
void afficher_booster(char type_booster, int x, int y);
void afficher_ennemi_joueur(joueur *J1, ennemi *E1);

/*_____Fonction d'affichage de la map_____*/
void afficher_map(map *map);

/*_____Fonstion d'affichage des mouvements_____*/
void afficher_avancer_joueur(int sens, joueur *J1);
void afficher_avancer_ennemi(ennemi *E1);

/*___Explotion de la bombe___*/
void afficher_centre_explosion(bombe *bombe);
void afficher_partie_explosion(sens sens, int degat, int x, int y);
void afficher_fin_explosion(sens sens, int degat, int x, int y);

/*__Fonctions disparition joueur/ennemi/mur*/
void afficher_mort_joueur(joueur *J1);
void afficher_mort_ennemi(ennemi *E1);
void afficher_ennemi_surpris(ennemi *E1);
void afficher_destruction_mur(int x, int y);

#endif
