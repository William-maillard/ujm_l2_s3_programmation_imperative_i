#ifndef DEF_MENUS_JEU_SOLO
#define DEF_MENUS_JEU_SOLO

void actualiser_paneau_statistiques(joueur *J1, int nombre_ennemis, int tempsActuel);

void bouton_pause();

void compte_a_rebour();

int menu_pause(char *partie, map map, joueur J1, all_ennemis mechants, int tempsActuel);

/*menu_fin_jeu:
  1.entree: -1 si tue par un ennemi, 0 si mort a cause du temps, 1 si gagne.
  2.sortie: 1 si click sur rejouer, O si click sur retour_menu.
*/
int menu_fin_jeu(int fin, int tempsFinal);

#endif
