/*
MANQUE MORT ENNEMI
Ce fichier contient toutes les declarations des variables d'images, d'animations et de lecteurs d'animation necessaire au jeu bomberman.
Pour abreger les nom des variables on utilise les suffixes suivant(1�lettre):
-p -> player
-e -> ennemi
-w -> wall
(lettre suivante)
-a -> animation
-ap -> animation player
 */
#ifndef _IMAGES_ANIMATIONS_H_
#define _IMAGES_ANIMATIONS_H_

/*____________________Declaration des variables____________________*/
extern int caseW, caseH; /*longueur/largeur d'une case*/
extern int temps;
/*images de base*/
extern MLV_Image *image;
extern MLV_Image *plateau;

/*Boosters*/
extern MLV_Image *boost_bombe, *boost_speed, *boost_life, *boost_range, *boost_damage, *boost_poison;

/*Joueur*/
extern MLV_Image *pdown[3], *pright[3], *pleft[3], *pup[3], *pdeath[7];
extern MLV_Animation *padown, *paright, *paleft, *paup, *padeath;
extern MLV_Animation_player *papdown, *papright, *papleft, *papup, *papdeath;

/*obstacle*/
extern MLV_Image *wall, *vide, *breakwall[7];
extern MLV_Animation *wabreak;
extern MLV_Animation_player *wapbreak;

/*bombe*/
extern MLV_Image *bombes[3], *explosion[4];
extern MLV_Image *explosion0up[2], *explosion0down[2], *explosion0left[2], *explosion0right[2];
extern MLV_Image *explosion1up[2], *explosion1down[2], *explosion1left[2], *explosion1right[2];
extern MLV_Image *explosion2up[2], *explosion2down[2], *explosion2left[2], *explosion2right[2];
extern MLV_Image *explosion3up[2], *explosion3down[2], *explosion3left[2], *explosion3right[2];

/*Ennemi*/
extern MLV_Image *ehorizontal[7], *evertical[7], *ehdeath[4], *evdeath[5];
extern MLV_Animation *eahorizontal_right, *eahorizontal_left, *eavertical_up, *eavertical_down, *ehadeath, *evadeath;
extern MLV_Animation_player *eaphorizontal_right, *eaphorizontal_left, *eapvertical_up, *eapvertical_down, *ehapdeath, *evapdeath;

/*___________________Declaration des fonctions___________________*/
void loading_images_animations();
void release_memory();
#endif
