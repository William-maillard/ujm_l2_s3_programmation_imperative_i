/**
 * Fichier contenant les declarations des fonctions des actions du joueur.
 ***/
#ifndef _JOUEUR_H_
#define _JOUEUR_H_

/*___booster___*/
void ramasser_booster(joueur *J1, map *map, int x, int y);
void drop_booster(map *map, int x, int y);

/*___joueur___*/
int avancer_joueur(sens sens, map *map, joueur *J1);
void joueur_perd_pv(joueur *J1, bombe *bombe, map *map);

/*___bombe___*/
bombe *poser_bombe(joueur *J1, map *map);
void detruire_obstacle(map *map, int x, int y);
void exploser_bombe(bombe *bombe, map *map);

#endif
