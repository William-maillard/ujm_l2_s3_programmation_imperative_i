/*
Declarations des fonctions des actions de l'ennemi.
*/
#ifndef _ENNEMI_H_
#define _ENNEMI_H_

void avancer_ennemi(ennemi *ennemi, map *map);
void ennemi_perd_pv(ennemi *E1, bombe *bombe, map *map);


#endif
