/******************************************************************
 *                                                                *
 * Fichier contenant tous les types nécessaires au jeu bomberman. *
 *                                                                *
 ******************************************************************/
#ifndef _TYPE_H_
#define _TYPE_H_

#define NBR_OBSTACLES 50
#define W 31 /*nbr de cases en largeur*/
#define H 13 /*nbr de cases en longueur*/
#define MAXPVJOUEUR 5
#define MAXBOMBES 4  /*le joueur ne pourra pas poser plus de 4 bombes*/
#define MAXENNEMIS 6 /*6 ennemis*/
#define MAXPORTEE 5
/*macro pour rendre le booster du type case plus lisible*/
#define AUCUN 'a'
#define VITESSE 'v'
#define POISON 'p'
#define DEGAT 'd'
#define LIFE 'l'
#define PORTEE 'r'
#define ADD_BOMBE 'b'

/*___type coordonnee___*/
typedef struct
{
  unsigned int x, y;
} coordonnees; /*origine en bas a droite*/

/*_____type bombe_____*/
typedef enum
{
  INACTIVE,
  ACTIVE
} etat_bombe;

typedef struct
{
  unsigned short int degat, portee;
  etat_bombe etat_bombe;
  int timer;
  coordonnees coordonnees;
} bombe;

/*__________type joueur__________*/
typedef struct
{
  unsigned short int pv, nombre_bombes;
  bombe bombes[MAXBOMBES]; /*contient les bombes que pose le joueur, on commence par les poser en partant de nombre bombe, quand nombre_bombes=0 on ne peut pas en poser*/
  coordonnees coordonnees;
  int vitesse; /*1 par defaut*/
} joueur;

/*___________type obstacle___________*/
typedef enum
{
  DELIMITEUR,
  MUR
} obstacle;

/*_____type ennemi_____*/
typedef enum
{
  UP,
  DOWN,
  LEFT,
  RIGHT
} sens;

typedef struct
{
  short unsigned int pv;
  coordonnees coordonnees;
  sens sens;
} ennemi;

typedef ennemi all_ennemis[MAXENNEMIS];

/*__________type map__________*/

typedef enum
{
  VIDE,
  OBSTACLE,
  JOUEUR,
  BOMBE,
  ENNEMI,
  BOOSTER,
  ENNEMI_JOUEUR,
  ENNEMI_BOOSTER
} etat_case;

typedef struct
{
  etat_case etat_case;
  obstacle obstacle;
  joueur *joueur;
  bombe *bombe;
  ennemi *ennemi;
  char booster;
} cases;
/*
booster:
  -v: ajoute +1 a la vitesse du perso ~~ tjrs pas compris comment definir la vitesse du perso
  -p: si on arrive a mettre un boost de vitesse pk pas mettre un poison->reduit vitesse +perte de vie / ou changer les commandes de deplacement ?
  -d: +1 degat a la bombe du perso
  -l: +1 pv au perso
  -r: +1 a la portee de la bombe
  -b: possibilit� de poser 1 bombe de plus en meme temps
*/
typedef struct
{
  cases pos[W][H];
} map;
/*faire une structure va nous permetre de faire une fonction initialiser_map qui renvoi une map*/

#endif
