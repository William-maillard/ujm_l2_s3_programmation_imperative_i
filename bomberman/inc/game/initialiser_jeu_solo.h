/*
Declaration des fonctions pour initialiser une partie solo de bomberman.
*/
#ifndef _INITIALISER_JEU_SOLO_H_
#define _INITIALISER_JEU_SOLO_H_
map initialiser_map();

joueur initialiser_joueur(map *map);

void initialiser_ennemis(all_ennemis mechants, map *map);

#endif