/*Les fonctions renvoie 1 si l'ouverture du fichier est reussi, -1 sinon*/
#ifndef _SAUVEGARDE_H_
#define _SAUVEGARDE_H_

#include "../characters/types.h"

int sauvegarder_partie(char *partie, map map, joueur J1, all_ennemis mechants, int tempsActuel);

int charger_partie(char *partie, map *map, joueur *J1, all_ennemis mechants, int *tempsMax);

#endif
