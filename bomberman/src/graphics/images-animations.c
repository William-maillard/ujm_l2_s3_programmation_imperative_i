/*
Fichier contenant les variables globales images, animations, lecteurs.
Et une fonctions pour charger les images/animations/lecteurs dans les variables, et une pour liberer la memoire.
*/
#include <MLV/MLV_all.h>
#include "../../inc/graphics/images-animations.h"
/*____________________Definition des variables____________________*/
int caseW = 36, caseH = 36; /*longueur/largeur d'une case*/
int temps = 1;              /*temps d'affichage des images dans les animations*/
/*---images de base---*/
MLV_Image *image = NULL;
MLV_Image *plateau = NULL;

/*---Boosters---*/
MLV_Image *boost_bombe, *boost_speed, *boost_life, *boost_range, *boost_damage, *boost_poison;

/*---Joueur---*/
MLV_Image *pdown[3], *pright[3], *pleft[3], *pup[3], *pdeath[7];
MLV_Animation *padown, *paright, *paleft, *paup, *padeath;
MLV_Animation_player *papdown, *papright, *papleft, *papup, *papdeath;

/*---obstacle--*/
MLV_Image *wall, *vide, *breakwall[7];
MLV_Animation *wabreak;
MLV_Animation_player *wapbreak;

/*---bombe---*/
MLV_Image *bombes[3], *explosion[4];
MLV_Image *explosion0up[2], *explosion0down[2], *explosion0left[2], *explosion0right[2];
MLV_Image *explosion1up[2], *explosion1down[2], *explosion1left[2], *explosion1right[2];
MLV_Image *explosion2up[2], *explosion2down[2], *explosion2left[2], *explosion2right[2];
MLV_Image *explosion3up[2], *explosion3down[2], *explosion3left[2], *explosion3right[2];

/*---Ennemi---*/
MLV_Image *ehorizontal[7], *evertical[7], *ehdeath[4], *evdeath[5];
MLV_Animation *eahorizontal_right, *eahorizontal_left, *eavertical_up, *eavertical_down, *ehadeath, *evadeath;
MLV_Animation_player *eaphorizontal_right, *eaphorizontal_left, *eapvertical_up, *eapvertical_down, *ehapdeath, *evapdeath;

/*______________________Fonction pour affecter les images/animations/lecteurs au variables_____________________*/
void loading_images_animations()
{
  /*______Chargement et "decoupage" de l'image______*/

  /*---images de base---*/
  if ((image = MLV_load_image("./bomberman/medias/sprites_bomberman.jpeg")) == NULL)
  {
    printf("Erreur de chargement de l'image: sprites_bomberman.jpeg\n");
  }
  if ((plateau = MLV_load_image("./bomberman/medias/bomberman_base-map.png")) == NULL)
  {
    printf("Erreur de chargement de l'image: bomberman_base-map.png\n");
  }

  /*-----Boosters-----*/
  boost_bombe = MLV_copy_partial_image(image, 0, 224, 16, 15);
  boost_speed = MLV_copy_partial_image(image, 48, 224, 16, 15);
  boost_life = MLV_copy_partial_image(image, 64, 224, 16, 15);
  boost_range = MLV_copy_partial_image(image, 80, 224, 16, 15);
  boost_damage = MLV_copy_partial_image(image, 96, 224, 16, 15);
  boost_poison = MLV_copy_partial_image(image, 160, 224, 16, 15);

  /*---Joueur---*/
  pdeath[0] = MLV_copy_partial_image(image, 0, 32, 15, 16);
  pdeath[1] = MLV_copy_partial_image(image, 16, 32, 15, 16);
  pdeath[2] = MLV_copy_partial_image(image, 32, 32, 15, 16);
  pdeath[3] = MLV_copy_partial_image(image, 48, 32, 15, 16);
  pdeath[4] = MLV_copy_partial_image(image, 64, 32, 15, 16);
  pdeath[5] = MLV_copy_partial_image(image, 80, 32, 15, 16);
  pdeath[6] = MLV_copy_partial_image(image, 96, 32, 15, 16);
  pleft[0] = MLV_copy_partial_image(image, 0, 0, 15, 15);
  pleft[2] = MLV_copy_partial_image(image, 15, 0, 15, 15);
  pleft[1] = MLV_copy_partial_image(image, 32, 1, 15, 15);
  pdown[0] = MLV_copy_partial_image(image, 49, 0, 15, 16);
  pdown[2] = MLV_copy_partial_image(image, 64, 0, 15, 16);
  pdown[1] = MLV_copy_partial_image(image, 79, 0, 16, 16);
  pright[0] = MLV_copy_partial_image(image, 1, 16, 15, 15);
  pright[2] = MLV_copy_partial_image(image, 17, 16, 15, 15);
  pright[1] = MLV_copy_partial_image(image, 32, 16, 15, 15);
  pup[0] = MLV_copy_partial_image(image, 49, 16, 15, 15);
  pup[2] = MLV_copy_partial_image(image, 64, 16, 15, 15);
  pup[1] = MLV_copy_partial_image(image, 80, 16, 15, 15);

  /*---obstacle---*/
  wall = MLV_copy_partial_image(image, 64, 48, 16, 16);
  vide = MLV_copy_partial_image(image, 5, 70, 1, 1);
  breakwall[0] = MLV_copy_partial_image(image, 64, 48, 16, 16);
  breakwall[1] = MLV_copy_partial_image(image, 80, 48, 16, 16);
  breakwall[2] = MLV_copy_partial_image(image, 96, 48, 16, 16);
  breakwall[3] = MLV_copy_partial_image(image, 112, 48, 16, 16);
  breakwall[4] = MLV_copy_partial_image(image, 128, 48, 16, 16);
  breakwall[5] = MLV_copy_partial_image(image, 144, 48, 16, 16);
  breakwall[6] = MLV_copy_partial_image(image, 160, 48, 16, 16);

  /*---bombe---*/
  bombes[2] = MLV_copy_partial_image(image, 0, 48, 17, 17);
  bombes[1] = MLV_copy_partial_image(image, 16, 48, 17, 17);
  bombes[0] = MLV_copy_partial_image(image, 31, 48, 17, 17);

  explosion[0] = MLV_copy_partial_image(image, 32, 97, 16, 16);
  explosion[1] = MLV_copy_partial_image(image, 112, 97, 16, 16);
  explosion[2] = MLV_copy_partial_image(image, 32, 176, 16, 16);
  explosion[3] = MLV_copy_partial_image(image, 111, 176, 16, 16);

  explosion0up[0] = MLV_copy_partial_image(image, 32, 81, 16, 16);
  explosion0up[1] = MLV_copy_partial_image(image, 32, 65, 16, 16);
  explosion0down[0] = MLV_copy_partial_image(image, 32, 110, 16, 16);
  explosion0down[1] = MLV_copy_partial_image(image, 32, 125, 16, 16);
  explosion0left[1] = MLV_copy_partial_image(image, 4, 97, 16, 16);
  explosion0left[0] = MLV_copy_partial_image(image, 20, 97, 16, 16);
  explosion0right[0] = MLV_copy_partial_image(image, 48, 97, 16, 16);
  explosion0right[1] = MLV_copy_partial_image(image, 64, 97, 16, 16);

  explosion1up[0] = MLV_copy_partial_image(image, 112, 81, 16, 16);
  explosion1up[1] = MLV_copy_partial_image(image, 112, 65, 16, 16);
  explosion1down[0] = MLV_copy_partial_image(image, 112, 110, 16, 16);
  explosion1down[1] = MLV_copy_partial_image(image, 112, 125, 16, 16);
  explosion1left[1] = MLV_copy_partial_image(image, 82, 97, 16, 16);
  explosion1left[0] = MLV_copy_partial_image(image, 98, 97, 16, 16);
  explosion1right[0] = MLV_copy_partial_image(image, 128, 97, 16, 16);
  explosion1right[1] = MLV_copy_partial_image(image, 142, 97, 16, 16);

  explosion2up[0] = MLV_copy_partial_image(image, 32, 160, 16, 16);
  explosion2up[1] = MLV_copy_partial_image(image, 32, 144, 16, 16);
  explosion2down[0] = MLV_copy_partial_image(image, 32, 192, 16, 16);
  explosion2down[1] = MLV_copy_partial_image(image, 32, 208, 16, 16);
  explosion2left[1] = MLV_copy_partial_image(image, 0, 176, 16, 16);
  explosion2left[0] = MLV_copy_partial_image(image, 16, 176, 16, 16);
  explosion2right[0] = MLV_copy_partial_image(image, 48, 176, 16, 16);
  explosion2right[1] = MLV_copy_partial_image(image, 64, 176, 16, 16);

  explosion3up[0] = MLV_copy_partial_image(image, 112, 160, 16, 16);
  explosion3up[1] = MLV_copy_partial_image(image, 112, 144, 16, 16);
  explosion3down[0] = MLV_copy_partial_image(image, 112, 192, 16, 16);
  explosion3down[1] = MLV_copy_partial_image(image, 112, 208, 16, 16);
  explosion3left[1] = MLV_copy_partial_image(image, 82, 176, 16, 16);
  explosion3left[0] = MLV_copy_partial_image(image, 98, 176, 16, 16);
  explosion3right[0] = MLV_copy_partial_image(image, 128, 176, 16, 16);
  explosion3right[1] = MLV_copy_partial_image(image, 142, 176, 16, 16);

  /*------ennemi------*/
  ehorizontal[0] = MLV_copy_partial_image(image, 96, 272, 16, 16);
  ehorizontal[1] = MLV_copy_partial_image(image, 80, 272, 16, 16);
  ehorizontal[2] = MLV_copy_partial_image(image, 64, 272, 16, 16);
  ehorizontal[3] = MLV_copy_partial_image(image, 48, 272, 16, 16);
  ehorizontal[4] = MLV_copy_partial_image(image, 32, 272, 16, 16);
  ehorizontal[5] = MLV_copy_partial_image(image, 16, 272, 16, 16);
  ehorizontal[6] = MLV_copy_partial_image(image, 0, 272, 16, 16);
  evertical[0] = MLV_copy_partial_image(image, 96, 256, 16, 16);
  evertical[1] = MLV_copy_partial_image(image, 80, 256, 16, 16);
  evertical[2] = MLV_copy_partial_image(image, 64, 256, 16, 16);
  evertical[3] = MLV_copy_partial_image(image, 48, 256, 16, 16);
  evertical[4] = MLV_copy_partial_image(image, 32, 256, 16, 16);
  evertical[5] = MLV_copy_partial_image(image, 16, 256, 16, 16);
  evertical[6] = MLV_copy_partial_image(image, 0, 256, 16, 16);

  ehdeath[0] = MLV_copy_partial_image(image, 112, 272, 16, 16);
  ehdeath[1] = MLV_copy_partial_image(image, 128, 272, 16, 16);
  ehdeath[2] = MLV_copy_partial_image(image, 144, 272, 16, 16);
  ehdeath[3] = MLV_copy_partial_image(image, 160, 272, 16, 16);
  evdeath[0] = MLV_copy_partial_image(image, 96, 304, 16, 16);
  evdeath[1] = MLV_copy_partial_image(image, 112, 288, 16, 16);
  evdeath[2] = MLV_copy_partial_image(image, 128, 288, 16, 16);
  evdeath[3] = MLV_copy_partial_image(image, 144, 288, 16, 16);
  evdeath[4] = MLV_copy_partial_image(image, 160, 288, 16, 16);

  /*__________________________________________Redimension des images_________________________________________*/

  /*---images de base---*/
  MLV_resize_image_with_proportions(plateau, caseW * 31, caseW * 13);

  /*---booster---*/
  MLV_resize_image_with_proportions(boost_bombe, caseW, caseH);
  MLV_resize_image_with_proportions(boost_speed, caseW, caseH);
  MLV_resize_image_with_proportions(boost_life, caseW, caseH);
  MLV_resize_image_with_proportions(boost_range, caseW, caseH);
  MLV_resize_image_with_proportions(boost_damage, caseW, caseH);
  MLV_resize_image_with_proportions(boost_poison, caseW, caseH);

  /*---joueur---*/
  MLV_resize_image_with_proportions(pdeath[0], caseW, caseH);
  MLV_resize_image_with_proportions(pdeath[1], caseW, caseH);
  MLV_resize_image_with_proportions(pdeath[2], caseW, caseH);
  MLV_resize_image_with_proportions(pdeath[3], caseW, caseH);
  MLV_resize_image_with_proportions(pdeath[4], caseW, caseH);
  MLV_resize_image_with_proportions(pdeath[5], caseW, caseH);
  MLV_resize_image_with_proportions(pdeath[6], caseW, caseH);
  MLV_resize_image_with_proportions(pleft[2], caseW, caseH);
  MLV_resize_image_with_proportions(pleft[0], caseW, caseH);
  MLV_resize_image_with_proportions(pleft[1], caseW, caseH);
  MLV_resize_image_with_proportions(pdown[2], caseW, caseH);
  MLV_resize_image_with_proportions(pdown[0], caseW, caseH);
  MLV_resize_image_with_proportions(pdown[1], caseW, caseH);
  MLV_resize_image_with_proportions(pright[2], caseW, caseH);
  MLV_resize_image_with_proportions(pright[0], caseW, caseH);
  MLV_resize_image_with_proportions(pright[1], caseW, caseH);
  MLV_resize_image_with_proportions(pup[2], caseW, caseH);
  MLV_resize_image_with_proportions(pup[0], caseW, caseH);
  MLV_resize_image_with_proportions(pup[1], caseW, caseH);

  /*---obstacle---*/
  MLV_resize_image_with_proportions(wall, caseW, caseH);
  MLV_resize_image_with_proportions(vide, caseW, caseH);
  MLV_resize_image_with_proportions(breakwall[0], caseW, caseH);
  MLV_resize_image_with_proportions(breakwall[1], caseW, caseH);
  MLV_resize_image_with_proportions(breakwall[2], caseW, caseH);
  MLV_resize_image_with_proportions(breakwall[3], caseW, caseH);
  MLV_resize_image_with_proportions(breakwall[4], caseW, caseH);
  MLV_resize_image_with_proportions(breakwall[5], caseW, caseH);
  MLV_resize_image_with_proportions(breakwall[6], caseW, caseH);

  /*---bombe---*/

  MLV_resize_image_with_proportions(bombes[0], caseW, caseH);
  MLV_resize_image_with_proportions(bombes[1], caseW, caseH);
  MLV_resize_image_with_proportions(bombes[2], caseW, caseH);

  MLV_resize_image_with_proportions(explosion[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion[2], caseW, caseH);
  MLV_resize_image_with_proportions(explosion[3], caseW, caseH);

  MLV_resize_image_with_proportions(explosion0up[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion0up[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion0down[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion0down[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion0left[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion0left[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion0right[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion0right[1], caseW, caseH);

  MLV_resize_image_with_proportions(explosion1up[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion1up[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion1down[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion1down[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion1left[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion1left[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion1right[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion1right[1], caseW, caseH);

  MLV_resize_image_with_proportions(explosion2up[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion2up[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion2down[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion2down[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion2left[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion2left[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion2right[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion2right[1], caseW, caseH);

  MLV_resize_image_with_proportions(explosion3up[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion3up[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion3down[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion3down[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion3left[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion3left[1], caseW, caseH);
  MLV_resize_image_with_proportions(explosion3right[0], caseW, caseH);
  MLV_resize_image_with_proportions(explosion3right[1], caseW, caseH);

  /*---ennemi---*/
  MLV_resize_image_with_proportions(ehorizontal[0], caseW, caseH);
  MLV_resize_image_with_proportions(ehorizontal[1], caseW, caseH);
  MLV_resize_image_with_proportions(ehorizontal[2], caseW, caseH);
  MLV_resize_image_with_proportions(ehorizontal[3], caseW, caseH);
  MLV_resize_image_with_proportions(ehorizontal[4], caseW, caseH);
  MLV_resize_image_with_proportions(ehorizontal[5], caseW, caseH);
  MLV_resize_image_with_proportions(ehorizontal[6], caseW, caseH);

  MLV_resize_image_with_proportions(evertical[0], caseW, caseH);
  MLV_resize_image_with_proportions(evertical[1], caseW, caseH);
  MLV_resize_image_with_proportions(evertical[2], caseW, caseH);
  MLV_resize_image_with_proportions(evertical[3], caseW, caseH);
  MLV_resize_image_with_proportions(evertical[4], caseW, caseH);
  MLV_resize_image_with_proportions(evertical[5], caseW, caseH);
  MLV_resize_image_with_proportions(evertical[6], caseW, caseH);

  MLV_resize_image_with_proportions(ehdeath[0], caseW, caseH);
  MLV_resize_image_with_proportions(ehdeath[1], caseW, caseH);
  MLV_resize_image_with_proportions(ehdeath[2], caseW, caseH);
  MLV_resize_image_with_proportions(ehdeath[3], caseW, caseH);

  MLV_resize_image_with_proportions(evdeath[0], caseW, caseH);
  MLV_resize_image_with_proportions(evdeath[1], caseW, caseH);
  MLV_resize_image_with_proportions(evdeath[2], caseW, caseH);
  MLV_resize_image_with_proportions(evdeath[3], caseW, caseH);
  MLV_resize_image_with_proportions(evdeath[4], caseW, caseH);

  /*______________________________________________Creation d'animations_______________________________________________*/

  /*---joueur---*/
  padeath = MLV_create_animation(7, 1, 1);
  MLV_add_frame_in_animation(&pdeath[0], NULL, temps, padeath);
  MLV_add_frame_in_animation(&pdeath[1], NULL, temps, padeath);
  MLV_add_frame_in_animation(&pdeath[2], NULL, temps, padeath);
  MLV_add_frame_in_animation(&pdeath[3], NULL, temps, padeath);
  MLV_add_frame_in_animation(&pdeath[4], NULL, temps, padeath);
  MLV_add_frame_in_animation(&pdeath[5], NULL, temps, padeath);
  MLV_add_frame_in_animation(&pdeath[6], NULL, temps, padeath);

  padown = MLV_create_animation(3, 1, 1);
  MLV_add_frame_in_animation(&pdown[0], NULL, temps, padown);
  MLV_add_frame_in_animation(&pdown[1], NULL, temps, padown);
  MLV_add_frame_in_animation(&pdown[2], NULL, temps, padown);

  paright = MLV_create_animation(3, 1, 1);
  MLV_add_frame_in_animation(&pright[0], NULL, temps, paright);
  MLV_add_frame_in_animation(&pright[1], NULL, temps, paright);
  MLV_add_frame_in_animation(&pright[2], NULL, temps, paright);

  paleft = MLV_create_animation(3, 1, 1);
  MLV_add_frame_in_animation(&pleft[0], NULL, temps, paleft);
  MLV_add_frame_in_animation(&pleft[1], NULL, temps, paleft);
  MLV_add_frame_in_animation(&pleft[2], NULL, temps, paleft);

  paup = MLV_create_animation(3, 1, 1);
  MLV_add_frame_in_animation(&pup[0], NULL, temps, paup);
  MLV_add_frame_in_animation(&pup[1], NULL, temps, paup);
  MLV_add_frame_in_animation(&pup[2], NULL, temps, paup);

  /*---obstacle---*/
  wabreak = MLV_create_animation(7, 1, 1);
  MLV_add_frame_in_animation(&breakwall[0], NULL, temps, wabreak);
  MLV_add_frame_in_animation(&breakwall[1], NULL, temps, wabreak);
  MLV_add_frame_in_animation(&breakwall[2], NULL, temps, wabreak);
  MLV_add_frame_in_animation(&breakwall[3], NULL, temps, wabreak);
  MLV_add_frame_in_animation(&breakwall[4], NULL, temps, wabreak);
  MLV_add_frame_in_animation(&breakwall[5], NULL, temps, wabreak);
  MLV_add_frame_in_animation(&breakwall[6], NULL, temps, wabreak);

  /*---ennemi---*/

  eahorizontal_right = MLV_create_animation(3, 1, 1);
  eahorizontal_left = MLV_create_animation(3, 1, 1);
  MLV_add_frame_in_animation(&ehorizontal[6], NULL, temps, eahorizontal_right);
  MLV_add_frame_in_animation(&ehorizontal[5], NULL, temps, eahorizontal_right);
  MLV_add_frame_in_animation(&ehorizontal[4], NULL, temps, eahorizontal_right);
  MLV_add_frame_in_animation(&ehorizontal[3], NULL, temps, eahorizontal_left);
  MLV_add_frame_in_animation(&ehorizontal[2], NULL, temps, eahorizontal_left);
  MLV_add_frame_in_animation(&ehorizontal[1], NULL, temps, eahorizontal_left);

  eavertical_down = MLV_create_animation(4, 1, 1);
  eavertical_up = MLV_create_animation(4, 1, 1);
  MLV_add_frame_in_animation(&evertical[6], NULL, temps, eavertical_down);
  MLV_add_frame_in_animation(&evertical[5], NULL, temps, eavertical_down);
  MLV_add_frame_in_animation(&evertical[4], NULL, temps, eavertical_down);
  MLV_add_frame_in_animation(&evertical[2], NULL, temps, eavertical_down);
  MLV_add_frame_in_animation(&evertical[1], NULL, temps, eavertical_up);
  MLV_add_frame_in_animation(&evertical[2], NULL, temps, eavertical_up);
  MLV_add_frame_in_animation(&evertical[4], NULL, temps, eavertical_up);
  MLV_add_frame_in_animation(&evertical[5], NULL, temps, eavertical_up);

  ehadeath = MLV_create_animation(5, 1, 1);
  MLV_add_frame_in_animation(&ehorizontal[0], NULL, temps, ehadeath);
  MLV_add_frame_in_animation(&ehdeath[0], NULL, temps, ehadeath);
  MLV_add_frame_in_animation(&ehdeath[1], NULL, temps, ehadeath);
  MLV_add_frame_in_animation(&ehdeath[2], NULL, temps, ehadeath);
  MLV_add_frame_in_animation(&ehdeath[3], NULL, temps, ehadeath);

  evadeath = MLV_create_animation(5, 1, 1);
  MLV_add_frame_in_animation(&evdeath[0], NULL, temps, evadeath);
  MLV_add_frame_in_animation(&evdeath[1], NULL, temps, evadeath);
  MLV_add_frame_in_animation(&evdeath[2], NULL, temps, evadeath);
  MLV_add_frame_in_animation(&evdeath[3], NULL, temps, evadeath);
  MLV_add_frame_in_animation(&evdeath[4], NULL, temps, evadeath);

  /*____________________________________Creation de lecteurs d'animation____________________________________*/

  /*---joueur---*/
  papdeath = MLV_create_animation_player(padeath);
  papdown = MLV_create_animation_player(padown);
  papright = MLV_create_animation_player(paright);
  papleft = MLV_create_animation_player(paleft);
  papup = MLV_create_animation_player(paup);

  /*---obstacle---*/
  wapbreak = MLV_create_animation_player(wabreak);

  /*---ennemi---*/
  eaphorizontal_right = MLV_create_animation_player(eahorizontal_right);
  eaphorizontal_left = MLV_create_animation_player(eahorizontal_left);
  eapvertical_down = MLV_create_animation_player(eavertical_down);
  eapvertical_up = MLV_create_animation_player(eavertical_up);
  ehapdeath = MLV_create_animation_player(ehadeath);
  evapdeath = MLV_create_animation_player(evadeath);
}

/*____________________________________Fonction pour liberer la memoire a la fin du jeu____________________________________*/

void release_memory()
{
  /*_____les images_____*/
  /*-bases-*/
  MLV_free_image(image);
  MLV_free_image(plateau);

  /*-booster-*/
  MLV_free_image(boost_bombe);
  MLV_free_image(boost_speed);
  MLV_free_image(boost_life);
  MLV_free_image(boost_range);
  MLV_free_image(boost_damage);
  MLV_free_image(boost_poison);

  /*---joueur---*/
  MLV_free_image(pdeath[0]);
  MLV_free_image(pdeath[1]);
  MLV_free_image(pdeath[2]);
  MLV_free_image(pdeath[3]);
  MLV_free_image(pdeath[4]);
  MLV_free_image(pdeath[5]);
  MLV_free_image(pdeath[6]);
  MLV_free_image(pdown[0]);
  MLV_free_image(pdown[1]);
  MLV_free_image(pdown[2]);
  MLV_free_image(pright[0]);
  MLV_free_image(pright[1]);
  MLV_free_image(pright[2]);
  MLV_free_image(pleft[0]);
  MLV_free_image(pleft[1]);
  MLV_free_image(pleft[2]);
  MLV_free_image(pup[0]);
  MLV_free_image(pup[1]);
  MLV_free_image(pup[2]);

  /*---obstacle---*/
  MLV_free_image(wall);
  MLV_free_image(breakwall[0]);
  MLV_free_image(breakwall[1]);
  MLV_free_image(breakwall[2]);
  MLV_free_image(breakwall[3]);
  MLV_free_image(breakwall[4]);
  MLV_free_image(breakwall[5]);
  MLV_free_image(breakwall[6]);

  /*---bombe---*/
  MLV_free_image(bombes[0]);
  MLV_free_image(bombes[1]);
  MLV_free_image(bombes[2]);
  MLV_free_image(explosion[0]);
  MLV_free_image(explosion[1]);
  MLV_free_image(explosion[2]);
  MLV_free_image(explosion[3]);

  MLV_free_image(explosion0up[0]);
  MLV_free_image(explosion0up[1]);
  MLV_free_image(explosion0down[0]);
  MLV_free_image(explosion0down[1]);
  MLV_free_image(explosion0left[0]);
  MLV_free_image(explosion0left[1]);
  MLV_free_image(explosion0right[0]);
  MLV_free_image(explosion0right[1]);

  MLV_free_image(explosion1up[0]);
  MLV_free_image(explosion1up[1]);
  MLV_free_image(explosion1down[0]);
  MLV_free_image(explosion1down[1]);
  MLV_free_image(explosion1left[0]);
  MLV_free_image(explosion1left[1]);
  MLV_free_image(explosion1right[0]);
  MLV_free_image(explosion1right[1]);

  MLV_free_image(explosion2up[0]);
  MLV_free_image(explosion2up[1]);
  MLV_free_image(explosion2down[0]);
  MLV_free_image(explosion2down[1]);
  MLV_free_image(explosion2left[0]);
  MLV_free_image(explosion2left[1]);
  MLV_free_image(explosion2right[0]);
  MLV_free_image(explosion2right[1]);

  MLV_free_image(explosion3up[0]);
  MLV_free_image(explosion3up[1]);
  MLV_free_image(explosion3down[0]);
  MLV_free_image(explosion3down[1]);
  MLV_free_image(explosion3left[0]);
  MLV_free_image(explosion3left[1]);
  MLV_free_image(explosion3right[0]);
  MLV_free_image(explosion3right[1]);

  /*---ennemi---*/
  MLV_free_image(ehorizontal[0]);
  MLV_free_image(ehorizontal[1]);
  MLV_free_image(ehorizontal[2]);
  MLV_free_image(ehorizontal[3]);
  MLV_free_image(ehorizontal[4]);
  MLV_free_image(ehorizontal[5]);
  MLV_free_image(ehorizontal[6]);

  MLV_free_image(evertical[0]);
  MLV_free_image(evertical[1]);
  MLV_free_image(evertical[2]);
  MLV_free_image(evertical[3]);
  MLV_free_image(evertical[4]);
  MLV_free_image(evertical[5]);
  MLV_free_image(evertical[6]);

  MLV_free_image(ehdeath[0]);
  MLV_free_image(ehdeath[1]);
  MLV_free_image(ehdeath[2]);
  MLV_free_image(ehdeath[3]);
  MLV_free_image(evdeath[0]);
  MLV_free_image(evdeath[1]);
  MLV_free_image(evdeath[2]);
  MLV_free_image(evdeath[3]);
  MLV_free_image(evdeath[4]);

  /*_____les animations_____*/

  /*---joueur---*/
  MLV_free_animation(padeath);
  MLV_free_animation(padown);
  MLV_free_animation(paright);
  MLV_free_animation(paleft);
  MLV_free_animation(paup);

  /*---obstacle---*/
  MLV_free_animation(wabreak);

  /*---ennemi---*/
  MLV_free_animation(eahorizontal_right);
  MLV_free_animation(eahorizontal_left);
  MLV_free_animation(eavertical_up);
  MLV_free_animation(eavertical_down);
  MLV_free_animation(ehadeath);
  MLV_free_animation(evadeath);

  /*_____les lecteurs_____*/

  /*---joueur---*/
  MLV_free_animation_player(papdeath);
  MLV_free_animation_player(papdown);
  MLV_free_animation_player(papleft);
  MLV_free_animation_player(papright);
  MLV_free_animation_player(papup);

  /*---obstacle---*/
  MLV_free_animation_player(wapbreak);

  /*---ennemi---*/
  MLV_free_animation_player(eaphorizontal_right);
  MLV_free_animation_player(eaphorizontal_left);
  MLV_free_animation_player(eapvertical_up);
  MLV_free_animation_player(eapvertical_down);
  MLV_free_animation_player(ehapdeath);
  MLV_free_animation_player(evapdeath);
}
