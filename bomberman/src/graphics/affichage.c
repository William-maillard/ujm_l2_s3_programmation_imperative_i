#include <MLV/MLV_all.h>

#include "../../inc/characters/types.h"
#include "../../inc/graphics/images-animations.h"

/*______fonction d'affichage de nos types______*/
void effacer(int x, int y)
{ /*attention les coordonnes passe en parametre de cette fonction sont les indices de map*caseW/caseH*/
  MLV_draw_image(vide, x, y);
  MLV_actualise_window();
}

void afficher_obstacle(int x, int y)
{
  MLV_draw_image(wall, caseW * x, caseH * y);
  MLV_actualise_window();
}

void afficher_joueur(int x, int y)
{
  MLV_draw_image(pdown[2], caseW * x, caseH * y);
  MLV_actualise_window();
}
void afficher_joueur_up(int x, int y)
{
  MLV_draw_image(pup[2], caseW * x, caseH * y);
  MLV_actualise_window();
}
void afficher_joueur_left(int x, int y)
{
  MLV_draw_image(pleft[2], caseW * x, caseH * y);
  MLV_actualise_window();
}
void afficher_joueur_right(int x, int y)
{
  MLV_draw_image(pright[2], caseW * x, caseH * y);
  MLV_actualise_window();
}

void afficher_bombe(int etat, bombe *bombe)
{
  int x = bombe->coordonnees.x, y = bombe->coordonnees.y;

  switch (etat)
  {
  case 1:
    MLV_draw_image(bombes[0], caseW * x, caseH * y);
    break;
  case 2:
    MLV_draw_image(bombes[1], caseW * x, caseH * y);
    break;
  case 3:
    MLV_draw_image(bombes[2], caseW * x, caseH * y);
    break;
  }
  MLV_actualise_window();
}

void afficher_ennemi(int sens, int x, int y)
{
  if (sens == UP || sens == DOWN)
  {
    MLV_draw_image(evertical[6], caseW * x, caseH * y);
  }
  else
  {
    MLV_draw_image(ehorizontal[5], caseW * x, caseH * y);
  }
  MLV_actualise_window();
}

void afficher_booster(char type_booster, int x, int y)
{
  switch (type_booster)
  {
  case VITESSE:
    MLV_draw_image(boost_speed, caseW * x, caseH * y);
    break;
  case POISON:
    MLV_draw_image(boost_poison, caseW * x, caseH * y);
    break;
  case DEGAT:
    MLV_draw_image(boost_damage, caseW * x, caseH * y);
    break;
  case LIFE:
    MLV_draw_image(boost_life, caseW * x, caseH * y);
    break;
  case PORTEE:
    MLV_draw_image(boost_range, caseW * x, caseH * y);
    break;
  case ADD_BOMBE:
    MLV_draw_image(boost_bombe, caseW * x, caseH * y);
    break;
  }
  MLV_actualise_window();
}

void afficher_ennemi_joueur(joueur *J1, ennemi *E1)
{
  /*idee dessine joueur sur la partie haute et ennemi sur la partie basse*/
  int x = J1->coordonnees.x, y = J1->coordonnees.y;

  MLV_draw_partial_image(pdown[2], 0, 0, caseW, 12, caseW * x, caseH * y);
  if (E1->sens == UP || E1->sens == DOWN)
  {
    MLV_draw_partial_image(evertical[0], 0, 12, caseW, caseH - 12, caseW * x, caseH * y + 12);
  }
  else
  {
    MLV_draw_partial_image(ehorizontal[0], 0, 12, caseW, caseH - 12, caseW * x, caseH * y + 12);
  }
  MLV_actualise_window();
}

/*_____Fonction d'affichage de la map_____*/

void afficher_map(map *map)
{
  int i, j;

  MLV_clear_window(MLV_COLOR_BLACK);
  MLV_draw_image(plateau, 0, 0);
  MLV_actualise_window();

  for (i = 1; i < 30; i++)
  {
    for (j = 1; j < 12; j++)
    {
      switch (map->pos[i][j].etat_case)
      {

      case OBSTACLE:
        if (map->pos[i][j].obstacle == MUR)
        {
          afficher_obstacle(i, j);
        }
        break;

      case JOUEUR:
        afficher_joueur(i, j);
        break;

      case BOMBE:
        afficher_bombe(1, map->pos[i][j].bombe);
        break;

      case ENNEMI:
        afficher_ennemi(map->pos[i][j].ennemi->sens, i, j);
        break;

      case BOOSTER:
        afficher_booster(map->pos[i][j].booster, i, j);
        break;
      case ENNEMI_JOUEUR:
        afficher_ennemi_joueur(map->pos[i][j].joueur, map->pos[i][j].ennemi);
        break;

      case ENNEMI_BOOSTER:
        afficher_ennemi(map->pos[i][j].ennemi->sens, i, j);
        break;

      case VIDE:
        break;
      }
      MLV_actualise_window();
    }
  }
}

/*_____Fonstion d'affichage des mouvements_____*/

void afficher_avancer_joueur(int sens, joueur *J1)
{
  int x = J1->coordonnees.x * caseW, y = J1->coordonnees.y * caseH;
  /*on passe les coordonee en fonction des indices, en coordonnees en fonction des cases*/
  int i, speed;

  /*
<Pour changer la vitesse de notre personnage il faut mettre +- d'images pour faire le deplacement en laissant le meme frame rate, ainsi le perso sera plus lent/rapide.
*/
  speed = 36 / J1->vitesse; /*J1->vitesse prend les valeurs: 1, 2(default), 3, 6*/

  MLV_change_frame_rate(18);
  switch (sens)
  {
  case UP:
    MLV_play_animation_player(papup);
    for (i = 0; i < speed; i++)
    {
      effacer(x, y); /*on efface le joueur*/
      y -= caseH / speed;
      MLV_draw_image_from_animation_player(papup, 0, x, y);
      MLV_update_animation_player(papup);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
    break;

  case DOWN:
    MLV_play_animation_player(papdown);
    for (i = 0; i < speed; i++)
    {
      effacer(x, y);
      y += caseH / speed;
      MLV_draw_image_from_animation_player(papdown, 0, x, y);
      MLV_update_animation_player(papdown);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
    break;

  case LEFT:
    MLV_play_animation_player(papleft);
    for (i = 0; i < speed; i++)
    {
      effacer(x, y);
      x -= caseW / speed;
      MLV_draw_image_from_animation_player(papleft, 0, x, y);
      MLV_update_animation_player(papleft);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
    break;

  case RIGHT:
    MLV_play_animation_player(papright);
    for (i = 0; i < speed; i++)
    {
      effacer(x, y);
      x += caseW / speed;
      MLV_draw_image_from_animation_player(papright, 0, x, y);
      MLV_update_animation_player(papright);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
    break;
  }
}

void afficher_avancer_ennemi(ennemi *E1)
{
  int x = E1->coordonnees.x * caseW, y = E1->coordonnees.y * caseH;
  int i;

  MLV_change_frame_rate(12);
  switch (E1->sens)
  {

  case UP:
    MLV_play_animation_player(eapvertical_up);
    for (i = 0; i < 12; i++)
    {
      effacer(x, y); /*on efface l'ennemi*/
      y -= caseH / 12;
      MLV_draw_image_from_animation_player(eapvertical_up, 0, x, y);
      MLV_update_animation_player(eapvertical_up);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
    break;

  case DOWN:
    MLV_play_animation_player(eapvertical_down);
    for (i = 0; i < 12; i++)
    {
      effacer(x, y);
      y += caseH / 12;
      MLV_draw_image_from_animation_player(eapvertical_down, 0, x, y);
      MLV_update_animation_player(eapvertical_down);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
    break;

  case LEFT:
    MLV_play_animation_player(eaphorizontal_left);
    for (i = 0; i < 12; i++)
    {
      effacer(x, y);
      x -= caseH / 12;
      MLV_update_animation_player(eaphorizontal_left);
      MLV_draw_image_from_animation_player(eaphorizontal_left, 0, x, y);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
    break;

  case RIGHT:
    MLV_play_animation_player(eaphorizontal_right);
    for (i = 0; i < 12; i++)
    {
      effacer(x, y);
      x += caseH / 12;
      MLV_update_animation_player(eaphorizontal_right);
      MLV_draw_image_from_animation_player(eaphorizontal_right, 0, x, y);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
    break;
  }
}

/*___Explotion de la bombe___*/

/*l'image de l'explosion depend des degats que fait la bombe de 0 a 4*/
/*Dans les fonctions qui suivent il n'y a (volontairement) pas de MLV_actualise_window(); l'affichage de l'explotion est gere par la fonction exploser_bombe de joueur.c*/
void afficher_centre_explosion(bombe *bombe)
{
  int x = bombe->coordonnees.x, y = bombe->coordonnees.y;

  switch (bombe->degat)
  {
  case 1:
    MLV_draw_image(explosion[0], caseW * x, caseH * y);
    break;
  case 2:
    MLV_draw_image(explosion[1], caseW * x, caseH * y);
    break;
  case 3:
    MLV_draw_image(explosion[2], caseW * x, caseH * y);
    break;
  case 4:
    MLV_draw_image(explosion[3], caseW * x, caseH * y);
    break;
  }
}

void afficher_partie_explosion(sens sens, int degat, int x, int y)
{
  switch (degat)
  {
  case 1:
    switch (sens)
    {
    case UP:
      MLV_draw_image(explosion0up[0], caseW * x, caseH * y);
      break;
    case DOWN:
      MLV_draw_image(explosion0down[0], caseW * x, caseH * y);
      break;
    case LEFT:
      MLV_draw_image(explosion0left[0], caseW * x, caseH * y);
      break;
    case RIGHT:
      MLV_draw_image(explosion0right[0], caseW * x, caseH * y);
      break;
    }
    break;

  case 2:
    switch (sens)
    {
    case UP:
      MLV_draw_image(explosion1up[0], caseW * x, caseH * y);
      break;
    case DOWN:
      MLV_draw_image(explosion1down[0], caseW * x, caseH * y);
      break;
    case LEFT:
      MLV_draw_image(explosion1left[0], caseW * x, caseH * y);
      break;
    case RIGHT:
      MLV_draw_image(explosion1right[0], caseW * x, caseH * y);
      break;
    }
    break;

  case 3:
    switch (sens)
    {
    case UP:
      MLV_draw_image(explosion2up[0], caseW * x, caseH * y);
      break;
    case DOWN:
      MLV_draw_image(explosion2down[0], caseW * x, caseH * y);
      break;
    case LEFT:
      MLV_draw_image(explosion2left[0], caseW * x, caseH * y);
      break;
    case RIGHT:
      MLV_draw_image(explosion2right[0], caseW * x, caseH * y);
      break;
    }
    break;

  case 4:
    switch (sens)
    {
    case UP:
      MLV_draw_image(explosion3up[0], caseW * x, caseH * y);
      break;
    case DOWN:
      MLV_draw_image(explosion3down[0], caseW * x, caseH * y);
      break;
    case LEFT:
      MLV_draw_image(explosion3left[0], caseW * x, caseH * y);
      break;
    case RIGHT:
      MLV_draw_image(explosion3right[0], caseW * x, caseH * y);
      break;
    }
    break;
  }
}

void afficher_fin_explosion(sens sens, int degat, int x, int y)
{
  switch (degat)
  {
  case 1:
    switch (sens)
    {
    case UP:
      MLV_draw_image(explosion0up[1], caseW * x, caseH * y);
      break;
    case DOWN:
      MLV_draw_image(explosion0down[1], caseW * x, caseH * y);
      break;
    case LEFT:
      MLV_draw_image(explosion0left[1], caseW * x, caseH * y);
      break;
    case RIGHT:
      MLV_draw_image(explosion0right[1], caseW * x, caseH * y);
      break;
    }
    break;

  case 2:
    switch (sens)
    {
    case UP:
      MLV_draw_image(explosion1up[1], caseW * x, caseH * y);
      break;
    case DOWN:
      MLV_draw_image(explosion1down[1], caseW * x, caseH * y);
      break;
    case LEFT:
      MLV_draw_image(explosion1left[1], caseW * x, caseH * y);
      break;
    case RIGHT:
      MLV_draw_image(explosion1right[1], caseW * x, caseH * y);
      break;
    }
    break;

  case 3:
    switch (sens)
    {
    case UP:
      MLV_draw_image(explosion2up[1], caseW * x, caseH * y);
      break;
    case DOWN:
      MLV_draw_image(explosion2down[1], caseW * x, caseH * y);
      break;
    case LEFT:
      MLV_draw_image(explosion2left[1], caseW * x, caseH * y);
      break;
    case RIGHT:
      MLV_draw_image(explosion2right[1], caseW * x, caseH * y);
      break;
    }
    break;

  case 4:
    switch (sens)
    {
    case UP:
      MLV_draw_image(explosion3up[1], caseW * x, caseH * y);
      break;
    case DOWN:
      MLV_draw_image(explosion3down[1], caseW * x, caseH * y);
      break;
    case LEFT:
      MLV_draw_image(explosion3left[1], caseW * x, caseH * y);
      break;
    case RIGHT:
      MLV_draw_image(explosion3right[1], caseW * x, caseH * y);
      break;
    }
    break;
  }
}

/*__Fonctions disparition joueur/ennemi/mur*/
void afficher_mort_joueur(joueur *J1)
{
  int x = J1->coordonnees.x, y = J1->coordonnees.y, i;

  MLV_play_animation_player(papdeath);
  MLV_change_frame_rate(7);
  for (i = 0; i < 7; i++)
  {
    MLV_draw_image_from_animation_player(papdeath, 0, caseW * x, caseH * y);
    MLV_update_animation_player(papdeath);
    MLV_actualise_window();
    MLV_delay_according_to_frame_rate();
  }
  effacer(caseW * x, caseH * y);
}

void afficher_mort_ennemi(ennemi *E1)
{
  int i;
  MLV_change_frame_rate(5);

  if (E1->sens == UP || E1->sens == DOWN)
  {
    for (i = 0; i < 5; i++)
    {
      MLV_play_animation_player(evapdeath);
      MLV_draw_image_from_animation_player(evapdeath, 0, caseW * E1->coordonnees.x, caseH * E1->coordonnees.y);
      MLV_update_animation_player(evapdeath);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
  }
  else
  {
    for (i = 0; i < 5; i++)
    {
      MLV_play_animation_player(ehapdeath);
      MLV_draw_image_from_animation_player(ehapdeath, 0, caseW * E1->coordonnees.x, caseH * E1->coordonnees.y);
      MLV_update_animation_player(ehapdeath);
      MLV_actualise_window();
      MLV_delay_according_to_frame_rate();
    }
  }
  effacer(caseW * E1->coordonnees.x, caseH * E1->coordonnees.y);
}

void afficher_ennemi_surpris(ennemi *E1)
{
  if (E1->sens == UP || E1->sens == DOWN)
  {
    MLV_draw_image(evertical[0], caseW * E1->coordonnees.x, caseH * E1->coordonnees.y);
  }
  else
  {
    MLV_draw_image(ehorizontal[0], caseW * E1->coordonnees.x, caseH * E1->coordonnees.y);
  }
}

void afficher_destruction_mur(int x, int y)
{
  int i;

  MLV_play_animation_player(wapbreak);
  MLV_change_frame_rate(21);
  for (i = 0; i < 7; i++)
  {
    effacer(caseW * x, caseH * y);
    MLV_draw_image_from_animation_player(wapbreak, 0, caseW * x, caseH * y);
    MLV_update_animation_player(wapbreak);
    MLV_actualise_window();
    MLV_delay_according_to_frame_rate();
  }
}
