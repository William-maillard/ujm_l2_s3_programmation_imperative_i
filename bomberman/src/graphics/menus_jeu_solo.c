/*
  Fichier contenant l'affichage des statistiques du jeu (pv J1, nombre_bombes, degat, portee, temps, nombre_ennemi, bouton pause);
  l'affichege du menu pause;
  et l'affichage de l'ecran de fin.
  Les grandeurs on ete prises arbitrairement a l'aide d'un capture d'ecran du jeu.
*/
#include <MLV/MLV_all.h>

#include "../../inc/characters/types.h"
#include "../../inc/game/sauvegarde.h"
#include "../../inc/game/meilleurs_scores.h"

void actualiser_paneau_statistiques(joueur *J1, int nombre_ennemis, int tempsActuel)
{
  int x = 20, y = 525, i, pvJ1 = J1->pv;

  /*_____partie gauche_____*/
  /*affichage de rectangle qui representes les pv de J1*/
  MLV_draw_adapted_text_box(20, 500, "pv_joueur:", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT);
  for (i = 0; i < MAXPVJOUEUR; i++)
  {
    if (pvJ1 != 0)
    {
      MLV_draw_filled_rectangle(x, y, 5, 30, MLV_COLOR_GREEN);
      pvJ1--;
    }
    else
    {
      MLV_draw_filled_rectangle(x, y, 5, 30, MLV_COLOR_GREY);
    }
    x += 10;
  }

  /*affichage du nombre de bombes de J1*/
  MLV_draw_adapted_text_box(20, 565, "nombre_bombes: %d", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, J1->nombre_bombes);

  /*affichage des degats et de la portee de la bombe*/
  MLV_draw_adapted_text_box(35, 585, "-degat_bombe: %d", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, J1->bombes[0].degat);
  MLV_draw_adapted_text_box(35, 605, "-portee_bombe: %d", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, J1->bombes[0].portee + 1);

  /*affichage de la vitesse du perso*/
  MLV_draw_adapted_text_box(20, 630, "vitesse_joueur: %d", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, J1->vitesse);

  /*_____partie droite_____*/
  /*affichage du temps et du nombre d'ennemis*/
  MLV_draw_adapted_text_box(910, 515, "temps: %d minutes %d secondes", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, (tempsActuel / 1000) / 60, (tempsActuel / 1000) % 60);
  MLV_draw_adapted_text_box(970, 530, "nombre_ennemis: %d", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, nombre_ennemis);

  /*On affiche les changements*/
  MLV_actualise_window();
}

void bouton_pause()
{
  /*dans un rectangle 100*100 de coordonne NO (985,600)*/
  MLV_draw_filled_rectangle(985, 600, 40, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(1045, 600, 40, 100, MLV_COLOR_GREEN);
  MLV_actualise_window();
}

void compte_a_rebour()
{
  /*3*/
  MLV_draw_filled_rectangle(540, 570, 30, 100, MLV_COLOR_RED); /*barre vertical du 3*/
  MLV_draw_filled_rectangle(440, 570, 100, 20, MLV_COLOR_RED); /*1iere barre horizontal*/
  MLV_draw_filled_rectangle(490, 610, 50, 20, MLV_COLOR_RED);  /*2ieme*/
  MLV_draw_filled_rectangle(440, 650, 100, 20, MLV_COLOR_RED); /*3ieme*/

  MLV_actualise_window();
  MLV_wait_seconds(1);
  MLV_draw_filled_rectangle(440, 570, 130, 100, MLV_COLOR_BLACK); /*on efface le nombre*/
  /*2*/
  MLV_draw_filled_rectangle(490, 570, 50, 20, MLV_COLOR_ORANGE); /*1iere barre horizontal*/
  MLV_draw_filled_rectangle(540, 570, 20, 50, MLV_COLOR_ORANGE); /*1iere barre vertical*/
  MLV_draw_filled_rectangle(490, 600, 50, 20, MLV_COLOR_ORANGE); /*2ieme barre horizontal*/
  MLV_draw_filled_rectangle(490, 620, 20, 30, MLV_COLOR_ORANGE); /*2ieme barre vertical*/
  MLV_draw_filled_rectangle(510, 630, 50, 20, MLV_COLOR_ORANGE); /*3ieme barre horizontal*/

  MLV_actualise_window();
  MLV_wait_seconds(1);
  MLV_draw_filled_rectangle(490, 570, 70, 80, MLV_COLOR_BLACK); /*on efface le nombre*/
  /*1*/
  MLV_draw_filled_rectangle(540, 570, 30, 100, MLV_COLOR_YELLOW);
  MLV_actualise_window();
  MLV_wait_seconds(1);

  MLV_draw_filled_rectangle(540, 570, 30, 100, MLV_COLOR_BLACK);
  MLV_actualise_window();
}

void afficher_pause()
{
  /*P*/
  MLV_draw_filled_rectangle(290, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(300, 500, 20, 10, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(300, 540, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(320, 500, 10, 60, MLV_COLOR_RED);
  /*A*/
  MLV_draw_filled_rectangle(340, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(350, 500, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(350, 540, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(370, 500, 10, 100, MLV_COLOR_RED);
  /*U*/
  MLV_draw_filled_rectangle(390, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(400, 590, 20, 10, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(420, 500, 10, 100, MLV_COLOR_RED);
  /*S*/
  MLV_draw_filled_rectangle(440, 500, 10, 60, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(450, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(450, 540, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(460, 560, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(440, 580, 40, 20, MLV_COLOR_RED);
  /*E*/
  MLV_draw_filled_rectangle(490, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(500, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(500, 540, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(500, 580, 30, 20, MLV_COLOR_RED);

  MLV_actualise_window();
}
/*retourne 1 si on quitte le jeu, 0 si on reprend*/
int menu_pause(char *partie, map map, joueur J1, all_ennemis mechants, int tempsActuel)
{
  int W_box1, H_box1, W_box2, H_box2, W_box3, H_box3; /*variables pour stocker les dimensions des boites de texte*/
  int x, y;                                           /*coordonnees du clic de la souris*/
  /*variables pour stocker les coordonnees max/min des boutons*/
  int xMax_bouton1, xMax_bouton2, xMax_bouton3, xMin_bouton3;
  int yMin_bouton2, yMin_bouton3;
  int yMax_bouton1, yMax_bouton2, yMax_bouton3;
  int fini = 0, quitter = 0;

  /*on efface les deux rectangles et on les remplaces par un carre rouge*/
  MLV_draw_filled_rectangle(985, 600, 100, 100, MLV_COLOR_RED);
  MLV_actualise_window();
  afficher_pause();

  /* On affiche les boutons cliquables:
    On dessine des boites de textes de dimension adaptee au texte,
    On recupere la dimension de la boite,
    On attend que le joueur clic sur une boite,
  */
  MLV_draw_adapted_text_box(475, 630, "Reprendre", 1, MLV_COLOR_GREEN, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_CENTER);
  MLV_get_size_of_adapted_text_box("Reprendre", 1, &W_box1, &H_box1);

  MLV_draw_adapted_text_box(475, 640 + H_box1, "Sauvegarder\net\nquitter", 1, MLV_COLOR_ORANGE, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_CENTER);
  MLV_get_size_of_adapted_text_box("Sauvegarder\net\nquitter", 1, &W_box2, &H_box2);

  MLV_draw_adapted_text_box(495 + W_box2, 640 + H_box1, "Retour-menu", 1, MLV_COLOR_RED, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_CENTER);
  MLV_get_size_of_adapted_text_box("Retour-menu", 1, &W_box3, &H_box3);

  MLV_actualise_window();

  xMax_bouton1 = 475 + W_box1;
  yMax_bouton1 = 630 + H_box1;
  xMax_bouton2 = 475 + W_box2;
  yMin_bouton2 = 640 + H_box1;
  yMax_bouton2 = yMin_bouton2 + H_box2;
  xMin_bouton3 = 495 + W_box2;
  xMax_bouton3 = xMin_bouton3 + W_box3;
  yMin_bouton3 = yMin_bouton2;
  yMax_bouton3 = yMax_bouton2;

  do
  {
    MLV_wait_mouse(&x, &y);
    if ((x >= 475 && x <= xMax_bouton1) && (y >= 630 && y <= yMax_bouton1))
    {           /*on a clic sur reprendre*/
      fini = 1; /*on a clic sur reprendre*/
    }
    else
    {
      if ((x >= 475 && x <= xMax_bouton2) && (y >= yMin_bouton2 && y <= yMax_bouton2))
      { /*on a clic sur sauvegarder*/
        sauvegarder_partie(partie, map, J1, mechants, tempsActuel);
        quitter = 1;
        fini = 1;
      }
      else
      {
        if ((x >= xMin_bouton3 && x <= xMax_bouton3) && (y >= yMin_bouton3 && y <= yMax_bouton3))
        { /*on a clic sur retour menu*/
          quitter = 1;
          fini = 1;
        }
      }
    }
  } while (fini == 0);

  /*On efface le menu pause et on redessine le bouton pause*/
  MLV_draw_filled_rectangle(985, 600, 100, 100, MLV_COLOR_BLACK);
  MLV_draw_filled_rectangle(290, 500, 450, 220, MLV_COLOR_BLACK);
  bouton_pause();
  MLV_actualise_window();

  return quitter;
}

/*___menu fin___*/

/*--afficher "game over", afficher "victoire"--*/
/*Cette portion de code n'est pas tres interessante a lire, on dessinne des rectangles pour afficher un message, on repete la fct 3 fois RGB,BRG, GBR en changeant la couleur des lettres pour pouvoir donner une impression de mouvement. fin L483*/
/*__________debut affichage "GAME OVER..."__________*/
void afficher_game_over_RGB()
{
  /*G*/
  MLV_draw_filled_rectangle(290, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(300, 500, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(300, 580, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(310, 540, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(320, 560, 10, 20, MLV_COLOR_RED);
  /*A*/
  MLV_draw_filled_rectangle(350, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(360, 500, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(360, 540, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(380, 500, 10, 100, MLV_COLOR_RED);
  /*M*/
  MLV_draw_filled_rectangle(410, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(420, 500, 10, 10, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(430, 510, 10, 10, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(440, 500, 10, 10, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(450, 500, 10, 100, MLV_COLOR_RED);
  /*E*/
  MLV_draw_filled_rectangle(470, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(480, 500, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(480, 540, 15, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(480, 580, 30, 20, MLV_COLOR_GREEN);
  /*O*/
  MLV_draw_filled_rectangle(540, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(550, 500, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(550, 580, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(580, 500, 10, 100, MLV_COLOR_GREEN);
  /*V*/
  MLV_draw_filled_rectangle(600, 500, 10, 60, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(610, 560, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(620, 580, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(630, 560, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(640, 500, 10, 60, MLV_COLOR_GREEN);
  /*E*/
  MLV_draw_filled_rectangle(660, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(670, 500, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(670, 540, 15, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(670, 580, 30, 20, MLV_COLOR_BLUE);
  /*R*/
  MLV_draw_filled_rectangle(710, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(720, 500, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(740, 520, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(720, 540, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(730, 560, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(740, 580, 10, 20, MLV_COLOR_BLUE);
  /*...*/
  MLV_draw_filled_rectangle(740, 620, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(770, 620, 20, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(800, 620, 20, 20, MLV_COLOR_BLUE);

  /*afichage des changements*/
  MLV_actualise_window();
}

void afficher_game_over_BRG()
{
  /*G*/
  MLV_draw_filled_rectangle(290, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(300, 500, 20, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(300, 580, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(310, 540, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(320, 560, 10, 20, MLV_COLOR_BLUE);
  /*A*/
  MLV_draw_filled_rectangle(350, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(360, 500, 20, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(360, 540, 20, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(380, 500, 10, 100, MLV_COLOR_BLUE);
  /*M*/
  MLV_draw_filled_rectangle(410, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(420, 500, 10, 10, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(430, 510, 10, 10, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(440, 500, 10, 10, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(450, 500, 10, 100, MLV_COLOR_BLUE);
  /*E*/
  MLV_draw_filled_rectangle(470, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(480, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(480, 540, 15, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(480, 580, 30, 20, MLV_COLOR_RED);
  /*O*/
  MLV_draw_filled_rectangle(540, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(550, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(550, 580, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(580, 500, 10, 100, MLV_COLOR_RED);
  /*V*/
  MLV_draw_filled_rectangle(600, 500, 10, 60, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(610, 560, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(620, 580, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(630, 560, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(640, 500, 10, 60, MLV_COLOR_RED);
  /*E*/
  MLV_draw_filled_rectangle(660, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(670, 500, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(670, 540, 15, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(670, 580, 30, 20, MLV_COLOR_GREEN);
  /*R*/
  MLV_draw_filled_rectangle(710, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(720, 500, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(740, 520, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(720, 540, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(730, 560, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(740, 580, 10, 20, MLV_COLOR_GREEN);
  /*...*/
  MLV_draw_filled_rectangle(740, 620, 20, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(770, 620, 20, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(800, 620, 20, 20, MLV_COLOR_GREEN);

  /*afichage des changements*/
  MLV_actualise_window();
}

void afficher_game_over_GBR()
{
  /*G*/
  MLV_draw_filled_rectangle(290, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(300, 500, 20, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(300, 580, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(310, 540, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(320, 560, 10, 20, MLV_COLOR_GREEN);
  /*A*/
  MLV_draw_filled_rectangle(350, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(360, 500, 20, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(360, 540, 20, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(380, 500, 10, 100, MLV_COLOR_GREEN);
  /*M*/
  MLV_draw_filled_rectangle(410, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(420, 500, 10, 10, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(430, 510, 10, 10, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(440, 500, 10, 10, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(450, 500, 10, 100, MLV_COLOR_GREEN);
  /*E*/
  MLV_draw_filled_rectangle(470, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(480, 500, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(480, 540, 15, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(480, 580, 30, 20, MLV_COLOR_BLUE);
  /*O*/
  MLV_draw_filled_rectangle(540, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(550, 500, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(550, 580, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(580, 500, 10, 100, MLV_COLOR_BLUE);
  /*V*/
  MLV_draw_filled_rectangle(600, 500, 10, 60, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(610, 560, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(620, 580, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(630, 560, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(640, 500, 10, 60, MLV_COLOR_BLUE);
  /*E*/
  MLV_draw_filled_rectangle(660, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(670, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(670, 540, 15, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(670, 580, 30, 20, MLV_COLOR_RED);
  /*R*/
  MLV_draw_filled_rectangle(710, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(720, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(740, 520, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(720, 540, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(730, 560, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(740, 580, 10, 20, MLV_COLOR_RED);
  /*...*/
  MLV_draw_filled_rectangle(740, 620, 20, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(770, 620, 20, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(800, 620, 20, 20, MLV_COLOR_RED);

  /*afichage des changements*/
  MLV_actualise_window();
}
/*__________Fin affichage "GAME OVER..."__________*/

/*__________Debut affichage "VICTOIRE!"__________*/
void afficher_victoire_RGB()
{
  /*V*/
  MLV_draw_filled_rectangle(290, 500, 10, 60, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(300, 560, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(310, 580, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(320, 560, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(330, 500, 10, 60, MLV_COLOR_RED);
  /*I*/
  MLV_draw_filled_rectangle(350, 500, 10, 100, MLV_COLOR_RED);
  /*C*/
  MLV_draw_filled_rectangle(370, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(380, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(380, 580, 30, 20, MLV_COLOR_RED);
  /*T*/
  MLV_draw_filled_rectangle(420, 500, 50, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(440, 520, 10, 80, MLV_COLOR_GREEN);
  /*O*/
  MLV_draw_filled_rectangle(480, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(490, 500, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(490, 580, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(520, 500, 10, 100, MLV_COLOR_GREEN);
  /*I*/
  MLV_draw_filled_rectangle(540, 500, 10, 100, MLV_COLOR_GREEN);
  /*R*/
  MLV_draw_filled_rectangle(560, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(570, 500, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(590, 520, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(570, 540, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(580, 560, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(590, 580, 10, 20, MLV_COLOR_BLUE);
  /*E*/
  MLV_draw_filled_rectangle(610, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(620, 500, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(620, 540, 15, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(620, 580, 30, 20, MLV_COLOR_BLUE);
  /*!*/
  MLV_draw_filled_rectangle(670, 500, 10, 60, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(670, 580, 10, 20, MLV_COLOR_BLUE);

  /*afichage des changements*/
  MLV_actualise_window();
}

void afficher_victoire_BRG()
{
  /*V*/
  MLV_draw_filled_rectangle(290, 500, 10, 60, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(300, 560, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(310, 580, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(320, 560, 10, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(330, 500, 10, 60, MLV_COLOR_BLUE);
  /*I*/
  MLV_draw_filled_rectangle(350, 500, 10, 100, MLV_COLOR_BLUE);
  /*C*/
  MLV_draw_filled_rectangle(370, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(380, 500, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(380, 580, 30, 20, MLV_COLOR_BLUE);
  /*T*/
  MLV_draw_filled_rectangle(420, 500, 50, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(440, 520, 10, 80, MLV_COLOR_RED);
  /*O*/
  MLV_draw_filled_rectangle(480, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(490, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(490, 580, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(520, 500, 10, 100, MLV_COLOR_RED);
  /*I*/
  MLV_draw_filled_rectangle(540, 500, 10, 100, MLV_COLOR_RED);
  /*R*/
  MLV_draw_filled_rectangle(560, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(570, 500, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(590, 520, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(570, 540, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(580, 560, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(590, 580, 10, 20, MLV_COLOR_GREEN);
  /*E*/
  MLV_draw_filled_rectangle(610, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(620, 500, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(620, 540, 15, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(620, 580, 30, 20, MLV_COLOR_GREEN);
  /*!*/
  MLV_draw_filled_rectangle(670, 500, 10, 60, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(670, 580, 10, 20, MLV_COLOR_GREEN);

  /*afichage des changements*/
  MLV_actualise_window();
}

void afficher_victoire_GBR()
{
  /*V*/
  MLV_draw_filled_rectangle(290, 500, 10, 60, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(300, 560, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(310, 580, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(320, 560, 10, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(330, 500, 10, 60, MLV_COLOR_GREEN);
  /*I*/
  MLV_draw_filled_rectangle(350, 500, 10, 100, MLV_COLOR_GREEN);
  /*C*/
  MLV_draw_filled_rectangle(370, 500, 10, 100, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(380, 500, 30, 20, MLV_COLOR_GREEN);
  MLV_draw_filled_rectangle(380, 580, 30, 20, MLV_COLOR_GREEN);
  /*T*/
  MLV_draw_filled_rectangle(420, 500, 50, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(440, 520, 10, 80, MLV_COLOR_BLUE);
  /*O*/
  MLV_draw_filled_rectangle(480, 500, 10, 100, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(490, 500, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(490, 580, 30, 20, MLV_COLOR_BLUE);
  MLV_draw_filled_rectangle(520, 500, 10, 100, MLV_COLOR_BLUE);
  /*I*/
  MLV_draw_filled_rectangle(540, 500, 10, 100, MLV_COLOR_BLUE);
  /*R*/
  MLV_draw_filled_rectangle(560, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(570, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(590, 520, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(570, 540, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(580, 560, 10, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(590, 580, 10, 20, MLV_COLOR_RED);
  /*E*/
  MLV_draw_filled_rectangle(610, 500, 10, 100, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(620, 500, 30, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(620, 540, 15, 20, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(620, 580, 30, 20, MLV_COLOR_RED);
  /*!*/
  MLV_draw_filled_rectangle(670, 500, 10, 60, MLV_COLOR_RED);
  MLV_draw_filled_rectangle(670, 580, 10, 20, MLV_COLOR_RED);

  /*afichage des changements*/
  MLV_actualise_window();
}
/*__________Fin affichage "VICTOIRE!"__________*/

/*__________Menu de fin de jeu__________*/
/*
  1.entree: -1 si tue par un ennemi, 0 si mort a cause du temps, 1 si gagne.
  2.sortie: 1 si click sur rejouer, O si click sur retour_menu.
*/
int menu_fin_jeu(int fin, int tempsFinal)
{
  int W_box1, H_box1, W_box2, H_box2, W_box3, H_box3; /*variables pour stocker les dimensions des boites de texte*/
  int couleur = 1;                                    /*variable pour faire changer le message de couleur*/
  int x, y;                                           /*coordonnees du clic de la souris*/
  /*variables pour stocker les coordonnees max/min des boutons*/
  int xMax_bouton1, xMax_bouton2, xMax_bouton3;
  int yMin_bouton2, yMin_bouton3;
  int yMax_bouton1, yMax_bouton2, yMax_bouton3;

  MLV_change_window_caption("Bomberman: fin-jeu", "[BOMBERMAN]");
  /*
    Si on a gagne, on test si le score est > a un meilleur score du top 10, si oui on demande le nom et affiche le tab des meilleurs scores sinon on passe.
    On affiche les boutons cliquables:
    On dessine des boites de textes de dimension adaptee au texte,
    On recupere la dimension de la boite,
    On attend que le joueur clic qq part,
  */
  if (fin == 1)
  {
    enregistrer_meilleur_score(tempsFinal); /*On test si le score est dans le top 10*/
  }

  MLV_draw_adapted_text_box(475, 630, "Rejouer", 1, MLV_COLOR_GREEN, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_CENTER);
  MLV_get_size_of_adapted_text_box("Rejouer", 1, &W_box1, &H_box1);

  MLV_draw_adapted_text_box(475, 640 + H_box1, "Meilleurs\nscores", 1, MLV_COLOR_GREY, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_CENTER);
  MLV_get_size_of_adapted_text_box("Meilleurs\nscores", 1, &W_box2, &H_box2);

  MLV_draw_adapted_text_box(475, 650 + H_box1 + H_box2, "Retour-menu", 1, MLV_COLOR_RED, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_CENTER);
  MLV_get_size_of_adapted_text_box("Retour-menu", 1, &W_box3, &H_box3);

  xMax_bouton1 = 475 + W_box1;
  yMax_bouton1 = 630 + H_box1;
  xMax_bouton2 = 475 + W_box2;
  yMin_bouton2 = 640 + H_box1;
  yMax_bouton2 = yMin_bouton2 + H_box2;
  xMax_bouton3 = 475 + W_box3;
  yMin_bouton3 = 650 + H_box1 + H_box2;
  yMax_bouton3 = 650 + H_box1 + H_box2 + H_box3;

  /*Si on a perdu on va mettre un message de la cause de la mort*/
  if (fin != 1)
  {
    if (fin == -1)
    {
      MLV_draw_adapted_text_box(730, 670, "Les ennemis on gagnes!\nvous ferez mieux la prochainne fois.\n ;)", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT);
    }
    else
    {
      MLV_draw_adapted_text_box(730, 670, "Vous avez manque de temps!", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT);
    }
  }
  else
  { /*on affiche le score*/
    MLV_draw_adapted_text_box(730, 670, "Vous avez gagne!\nscore: %dm %ds %dms", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, (tempsFinal / 100) / 60, (tempsFinal / 100) % 60, tempsFinal / 100);
  }

  /*On affiche "VICTOIRE!" ou "GAME OVER..." et on le change de couleur tant que le joueur ne clic pas sur rejouer/quitter*/
  if (fin == 1)
  {
    do
    {
      switch (couleur)
      {
      case 1:
        afficher_victoire_RGB();
        couleur++;
        break;
      case 2:
        afficher_victoire_BRG();
        couleur++;
        break;
      case 3:
        afficher_victoire_GBR();
        couleur = 1;
        break;
      }
      MLV_wait_mouse_or_seconds(&x, &y, 1);

      if ((x >= 475 && x <= xMax_bouton2) && (y >= yMin_bouton2 && y <= yMax_bouton2))
      { /*si on a apuye sur afficher score*/
        afficher_meilleurs_scores();
        /*appeler la fonction afficher score*/
      }
    } while (!((x >= 475 && x <= xMax_bouton1) && (y >= 630 && y <= yMax_bouton1)) && !((x >= 475 && x <= xMax_bouton3) && (y >= yMin_bouton3 && y <= yMax_bouton3)));
  }
  else
  {
    do
    {
      switch (couleur)
      {
      case 1:
        afficher_game_over_RGB();
        couleur++;
        break;
      case 2:
        afficher_game_over_BRG();
        couleur++;
        break;
      case 3:
        afficher_game_over_GBR();
        couleur = 1;
        break;
      }
      MLV_wait_mouse_or_seconds(&x, &y, 1);

      if ((x >= 475 && x <= xMax_bouton2) && (y >= yMin_bouton2 && y <= yMax_bouton2))
      { /*si on a apuye sur afficher score*/
        printf("scores!!\n");
        afficher_meilleurs_scores();
      }
    } while (!((x >= 475 && x <= xMax_bouton1) && (y >= 630 && y <= yMax_bouton1)) && !((x >= 475 && x <= xMax_bouton3) && (y >= yMin_bouton3 && y <= yMax_bouton3)));
  }
  if ((x >= 475 && x <= xMax_bouton1) && (y >= 630 && y <= yMax_bouton1))
  { /*on a clic sur rejouer*/
    return 1;
  }
  else
  {
    return 0;
  }
}
