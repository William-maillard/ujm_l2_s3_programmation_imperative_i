#include <stdio.h>
#include <stdlib.h>
#include "../../inc/characters/types.h"

/*_____On initialise la map_____*/
map initialiser_map()
{
  int i, j, obstacles_places = 0;
  map map;

  /*rand est initialise dans la fct jeu_solo*/
  /*On initialise les cases*/
  for (i = 0; i < W; i++)
  {
    for (j = 0; j < H; j++)
    {
      map.pos[i][j].etat_case = VIDE;
    }
  }
  /*On met les delimiteurs en bord de map*/
  for (j = 0; j < H; j++)
  {
    map.pos[0][j].etat_case = OBSTACLE;
    map.pos[0][j].obstacle = DELIMITEUR;
    map.pos[30][j].etat_case = OBSTACLE;
    map.pos[30][j].obstacle = DELIMITEUR;
  }
  for (i = 1; i < W - 1; i++)
  {
    map.pos[i][0].etat_case = OBSTACLE;
    map.pos[i][0].obstacle = DELIMITEUR;
    map.pos[i][12].etat_case = OBSTACLE;
    map.pos[i][12].obstacle = DELIMITEUR;
  }
  /*On place les delimiteurs de la carte*/
  for (i = 2; i < W - 1; i += 2)
  {
    for (j = 2; j < 12; j += 2)
    {
      map.pos[i][j].etat_case = OBSTACLE;
      map.pos[i][j].obstacle = DELIMITEUR;
    }
  }

  /*On place les obstacles aleatoirement\n*/
  while (obstacles_places != NBR_OBSTACLES)
  {
    i = (rand() % 29) + 2;
    j = (rand() % 11) + 1;
    if (i < 4 && j < 4)
    {
      /*on va faire un autre tour de boucle pour laisser un carre de 2*2 libre a la position de depart du joueur*/
    }
    else
    {
      if (map.pos[i][j].etat_case == VIDE)
      {
        map.pos[i][j].etat_case = OBSTACLE;
        map.pos[i][j].obstacle = MUR;
        obstacles_places++;
      }
    }
  }
  return map;
}

/*_____On initialise le joueur et ses bombes______*/
joueur initialiser_joueur(map *map)
{
  int i;
  joueur *J1 = malloc(sizeof(joueur));

  J1->pv = 2;
  J1->nombre_bombes = 1;
  J1->vitesse = 2;
  J1->coordonnees.x = 1;
  J1->coordonnees.y = 1;
  for (i = 0; i < MAXBOMBES; i++)
  {
    J1->bombes[i].degat = 1;
    J1->bombes[i].portee = 1;
    J1->bombes[i].timer = 0;
    J1->bombes[i].etat_bombe = INACTIVE;
  }
  /*--on place le joueur en (1;1)--*/
  map->pos[1][1].etat_case = JOUEUR;
  map->pos[1][1].joueur = J1;
  return *J1;
}

/*_____On initialise les ennemis_____*/
void initialiser_ennemis(all_ennemis mechants, map *map)
{
  int i, j, k;
  for (i = 0; i < MAXENNEMIS; i++)
  {
    if (i % 2 == 0)
    {
      mechants[i].pv = 4;
      mechants[i].sens = UP;
      /*printf("ennemin %d est V\n", i);*/
    }
    else
    {
      mechants[i].pv = 3;
      mechants[i].sens = LEFT;
      /*printf("ennemin %d est H\n", i);*/
    }
  }
  /*Pour pas que les ennemis pop a cote du joueur on ne les place qu'a partir de la 3 colonne i->[3;29]*/
  /*--On place les ennemis horizontal--*/
  /*pour etre sur que l'ennemi ne soit pas bloque entre deux delimiteur on va utiliser des indices i paires, car les delimiteurs de notre map se trouve a des indices i pair et si l'ennemi a un indice pair il ne pourra pas se touver entre deux delimiteurs*/
  for (k = 1; k < MAXENNEMIS; k += 2)
  {
    do
    {
      i = (rand() % 29) + 3;
      j = (rand() % 11) + 1;
      if (i % 2 != 0)
      { /*si i est pair*/
        i--;
      }
    } while (map->pos[i][j].etat_case != VIDE);

    map->pos[i][j].etat_case = ENNEMI;
    map->pos[i][j].ennemi = &mechants[k];
    mechants[k].coordonnees.x = i;
    mechants[k].coordonnees.y = j;
  }
  /*--On place les ennemis vertical--*/
  /*pour les memes raisons on va utiliser des indices j paires*/
  for (k = 0; k < MAXENNEMIS; k += 2)
  {
    do
    {
      i = (rand() % 29) + 3;
      j = (rand() % 11) + 1;
      if (j % 2 != 0)
      { /*si j est pair*/
        j--;
      }
    } while (map->pos[i][j].etat_case != VIDE);

    map->pos[i][j].etat_case = ENNEMI;
    map->pos[i][j].ennemi = &mechants[k];
    mechants[k].coordonnees.x = i;
    mechants[k].coordonnees.y = j;
  }
}
