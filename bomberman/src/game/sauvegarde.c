/*
  Pour demarer une partie on a besoin d'une map, d'un joueur, des ennemis.
  Pour sauvegarder une partie, nous allons donc sauvegarde ces 3 structures.
  Pour charger une partie, il nous faudra charger les valeurs des differents champs de ces structures stockees dans un fichier de sauvegarde.

  Pour l'instant, on ne peut sauvegarder qu'une partie
*/
#include <stdio.h>
#include <stdlib.h>
#include "../../inc/characters/types.h"
#include "../../inc/characters/joueur.h" /*pour la fct drop_booster*/

/*Les fonctions renvoie 1 si l'ouverture du fichier est reussi, -1 sinon*/

int sauvegarder_partie(char *partie, map map, joueur J1, all_ennemis mechants, int tempsActuel)
{
  FILE *sauvegarde = NULL;
  int i, j;

  /*ouvert en mode ecriture avec supression de contenu*/
  if ((sauvegarde = fopen(partie, "w+")) == NULL)
  {
    return -1;
  }
  printf("/*_____sauvegarde de la map_____*/\n");
  for (i = 0; i < W; i++)
  {
    for (j = 0; j < H; j++)
    {
      fprintf(sauvegarde, "%d ", map.pos[i][j].etat_case);
    }
    fputc('\n', sauvegarde);
  }

  printf("/*_____sauvegarde du joueur_____*/\n");
  fprintf(sauvegarde, "%hu %hu %d %u %u\n", J1.pv, J1.nombre_bombes, J1.vitesse, J1.coordonnees.x, J1.coordonnees.y);

  printf("/*_____sauvegarde des parametres des bombes_____*/\n");
  fprintf(sauvegarde, "%d %d\n", J1.bombes[0].degat, J1.bombes[0].portee);
  /*Remarque, on ne sauvegarde pas les bombes actives"*/

  printf("/*_____sauvegarde des ennemis_____*/\n");
  for (j = 0; j < MAXENNEMIS; j++)
  {
    fprintf(sauvegarde, "%hu", mechants[j].pv);
    if (mechants[j].pv != 0)
    { /*si l'ennemi est pas mort on stocke son sens et ses coordonnees*/
      fprintf(sauvegarde, " %u %u %u", mechants[j].sens, mechants[j].coordonnees.x, mechants[j].coordonnees.y);
    }
    fprintf(sauvegarde, "\n");
  }

  printf("/*_____On stocke le temps deja ecoule_____*/\n");
  fprintf(sauvegarde, "%d", tempsActuel);

  fclose(sauvegarde);
  return 1;
}

int charger_partie(char *partie, map *map, joueur *J1, all_ennemis mechants, int *tempsMax)
{
  FILE *sauvegarde = NULL;
  int i, j, temps;
  short unsigned int d, pv;
  unsigned int sens, x, y;
  printf("bienvenu dans la fonction charger partie\n");
  /*on ouvre le fichier en mode lecture*/
  if ((sauvegarde = fopen(partie, "r")) == NULL)
  {
    return -1;
  }

  printf(" /*_____chargement de la map_____*/\n");
  /*On recupere les etat case de notre map*/
  for (i = 0; i < W; i++)
  {
    for (j = 0; j < H; j++)
    {
      if (fscanf(sauvegarde, "%hu", &d) != 1)
      {
        printf("Probleme de chargement (%d,%d)\n", i, j);
      }
      map->pos[i][j].etat_case = d;
    }
  }

  printf("/*_____chargement du joueur_____*/\n");
  if (fscanf(sauvegarde, "%hu %hu %d %u %u\n", &J1->pv, &J1->nombre_bombes, &J1->vitesse, &J1->coordonnees.x, &J1->coordonnees.y) != 5)
  {
    printf("Probleme de chargement des donnees du joueur\n");
  }
  map->pos[J1->coordonnees.x][J1->coordonnees.y].joueur = J1;

  printf("/*_____chargement des bombes_____*/\n");
  if (fscanf(sauvegarde, "%hu %hu", &J1->bombes[0].degat, &J1->bombes[0].portee) != 2)
  {
    printf("Probleme de chargement des donnees de la bombe\n");
  }
  J1->bombes[0].etat_bombe = INACTIVE;

  for (i = 1; i < MAXBOMBES; i++)
  { /*ont met les meme valeurs dans les champs degat/portee de toutes les bombes du joueur*/
    J1->bombes[i].degat = J1->bombes[0].degat;
    J1->bombes[i].degat = J1->bombes[0].portee;
    J1->bombes[i].etat_bombe = INACTIVE;
  }

  printf(" /*_____chargement des ennemis_____*/\n");
  for (j = 0; j < MAXENNEMIS; j++)
  {
    if (fscanf(sauvegarde, "%hu", &pv) != 1)
    {
      printf("Probleme de chargement des pv du %d ennemi\n", j);
    }
    mechants[j].pv = pv;
    if (mechants[j].pv != 0)
    { /*si l'ennemi est pas mort on a son sens et ses coordonnees a recuperer, et on le place sur la map*/
      if (fscanf(sauvegarde, "%u %u %u", &sens, &x, &y) != 3)
      {
        printf("Probleme de chargement des donnees de l'ennemi %d\n", j);
      }
      mechants[j].sens = sens;
      mechants[j].coordonnees.x = x;
      mechants[j].coordonnees.y = y;
      map->pos[mechants[j].coordonnees.x][mechants[j].coordonnees.y].ennemi = &mechants[j];
    }
  }

  /*Rappel, on a desactive les bombes actives, ont va donc actualise les etat_cases de la map, on ne sauvegarde pas les boosters, et il faut qu'on replace les champs obstacle/booster des cases*/
  for (i = 0; i < W; i++)
  {
    for (j = 0; j < H; j++)
    {
      switch (map->pos[i][j].etat_case)
      {
      case BOMBE:
        map->pos[i][j].etat_case = VIDE;
        break;
      case BOOSTER:
        drop_booster(map, i, j); /*Fonction de joueur.c qui permet de drop un booster*/
        break;
      case ENNEMI_BOOSTER:
        map->pos[i][j].etat_case = ENNEMI;
        break;
      case OBSTACLE:
        if (i == 0 || j == 0)
        {
          map->pos[i][j].obstacle = DELIMITEUR;
        }
        else
        {
          if (i % 2 == 0 && j % 2 == 0)
          {
            map->pos[i][j].obstacle = DELIMITEUR;
          }
          else
          {
            map->pos[i][j].obstacle = MUR;
          }
        }
      default:
        break;
      }
    }
  }

  printf("/*_____On recupere le temps deja ecoule et on le deduis de tempsMax_____*/\n");
  if (fgetc(sauvegarde) != '\n')
  {
    printf("Probleme de chargement: manque un saut de ligne a l'avant derniere ligne\n");
  }
  if (fscanf(sauvegarde, "%d", &temps) != 1)
  {
    printf("Probleme de chargement du temps a la derniere ligne\n");
  }
  tempsMax -= temps;

  fclose(sauvegarde);
  return 1;
}
