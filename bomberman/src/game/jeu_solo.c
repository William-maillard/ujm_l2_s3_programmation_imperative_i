/*
Ce module contient la fonction pour jouer une partie solo.
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <MLV/MLV_all.h>

#include "../../inc/characters/types.h"
#include "../../inc/graphics/images-animations.h"
#include "../../inc/game/initialiser_jeu_solo.h"
#include "../../inc/graphics/affichage.h"
#include "../../inc/characters/joueur.h"
#include "../../inc/characters/ennemi.h"
#include "../../inc/graphics/menus_jeu_solo.h"
#include "../../inc/game/sauvegarde.h"

#define TEMPSDEJEUMAX 300000 /*5 minutes en millisecondes*/

void jeu_solo(char *sauvegarde)
{
	map carte;
	map *map;
	joueur J1;
	all_ennemis les_ennemis;
	bombe *bombes_actives[MAXBOMBES + 1]; /*+1 nous permet d'avoir toujours une case libre dans le tableau*/
	int quitter = 0;					  /*passe a 1 si on appuie sur quitter dans le menu pause*/
	int rejouer;
	int j, t; /*j est l'indice du tableau bombes_actives, t va stocker le timer de la bombe*/
	int k, nbr_ennemis = MAXENNEMIS;
	int tempsDepart, tempsActuel, tempsBougerEnnemis, tempsFinal = 0, tempsMax = TEMPSDEJEUMAX;
	MLV_Keyboard_button touche; /*code de la touche appuye ou relache*/
	MLV_Button_state etat;		/*pour savoir si une touche est appuye ou relache, ou le bouton de la souris click ou relache*/
	int xSouris, ySouris;		/*position de la souris*/
	MLV_Event event;

	srand(time(NULL));
	/*__On adapte la fenetre au jeu__*/
	MLV_change_window_size(31 * caseW, 20 * caseH);

	do
	{
		MLV_clear_window(MLV_COLOR_BLACK);

		for (j = 0; j < MAXBOMBES; j++)
		{ /*ils n'y a pas de bombes actives, on initialise avec des pointeurs NULL*/
			bombes_actives[j] = NULL;
		}
		tempsMax = TEMPSDEJEUMAX; /*____________Il faut reinitialise temps max (avant le chargement de la partie car le chargement modifie tempsMax) car si on met le jeu sur pause on modifie tempsMax*/
		/*___Doit-on charger une partie ?___*/
		if (strcmp(sauvegarde, "\0") != 0)
		{ /*si on a un chemin de fichier de sauvegarde passe en parametre*/
			if (charger_partie(sauvegarde, &carte, &J1, les_ennemis, &tempsMax) == -1)
			{
				MLV_draw_text(0, 0, "Echec du chargement du fichier de sauvegarde\n", MLV_COLOR_WHITE);
				MLV_actualise_window();
				MLV_wait_seconds(1);
				quitter = 1;
			}
			tempsFinal = 300000 - tempsMax; /*temps de la partie charge deja ecoule qui va compter pour le score final*/
		}
		else
		{ /*__Si non, on initialise nos variables__*/
			carte = initialiser_map();
			J1 = initialiser_joueur(map);
			initialiser_ennemis(les_ennemis, map);
		}
		map = &carte;

		/*__On charge en memoire les images/animations/lecteurs__*/
		loading_images_animations();

		/*__On affiche la map, le bouton pause et,on  demarre le compte a rebour__*/
		afficher_map(map);
		bouton_pause();
		compte_a_rebour();

		/*__On initialise nos differents timers__*/
		tempsDepart = MLV_get_time();
		tempsActuel = tempsDepart;
		tempsBougerEnnemis = tempsDepart; /*c'est le temps ou on a bouge nos ennemis*/

		actualiser_paneau_statistiques(&J1, nbr_ennemis, tempsActuel);

		while ((nbr_ennemis != 0) && (J1.pv != 0) && (tempsActuel <= tempsMax) && (quitter == 0))
		{
			/*On recupere un evenement et on test si il correspond a une touche d'action du joueur*/
			event = MLV_get_event(&touche, NULL, NULL, NULL, NULL, &xSouris, &ySouris, NULL, &etat);

			switch (event)
			{

			case MLV_KEY:
				if (etat == MLV_PRESSED)
				{ /*si on a apuye sur une touche*/
					switch (touche)
					{

					case MLV_KEYBOARD_UP:
						avancer_joueur(UP, map, &J1);
						break;

					case MLV_KEYBOARD_DOWN:
						avancer_joueur(DOWN, map, &J1);
						break;

					case MLV_KEYBOARD_LEFT:
						avancer_joueur(LEFT, map, &J1);
						break;

					case MLV_KEYBOARD_RIGHT:
						avancer_joueur(RIGHT, map, &J1);
						break;

					case MLV_KEYBOARD_SPACE:
						j = 0;
						while (bombes_actives[j] != NULL /*|| j<MAXBOMBES*/)
						{ /*on cherche une place libre dans le tableau*/
							j++;
						}
						bombes_actives[j] = poser_bombe(&J1, map); /*on place le pointeur de notre bombe*/
						break;

					case MLV_KEYBOARD_F1:
						/*si on a un bug d'affichage on peut raffraichir la map */
						afficher_map(map);
						break;

					default:
						break;
					}
					break;

				case MLV_MOUSE_BUTTON:
					if ((xSouris >= 985 && xSouris <= 1085) && (ySouris >= 600 && ySouris <= 700))
					{
						/*On va "stopper le temps"*/
						tempsMax -= tempsActuel;				/*le temps qu'il restera en reprenant la partie*/
						tempsFinal += TEMPSDEJEUMAX - tempsMax; /*c'est a dire le temps ecoule*/
						quitter = menu_pause("partieBomberman.txt", carte, J1, les_ennemis, tempsActuel);
						/*Si on ne quitte pas, on reprend la partie, il faut reinitialiser le tempsDepart et le tempsActuel*/
						if (quitter == 0)
						{
							compte_a_rebour();
							tempsDepart = MLV_get_time();
							tempsActuel = tempsDepart;
							/*On affiche le nouveau temps max qui n'est plus de 5 min*/
							MLV_draw_adapted_text_box(910, 495, "temps-max: %d m %d s", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, (tempsMax / 1000) / 60, (tempsMax / 1000) % 60);
						}
					}
					break;

				default:
					break;
				}
			}

			/*S'il y a des bombes actives on test si elles doivent "grossir" ou exploser*/
			for (j = 0; j < MAXBOMBES; j++)
			{
				if (bombes_actives[j] != NULL)
				{
					t = tempsActuel - (bombes_actives[j]->timer - tempsDepart); /*on va tester combien de temps c'est ecoule depuis le depot de la bombe*/
					if (t >= 3000)
					{
						exploser_bombe(bombes_actives[j], map);
						bombes_actives[j]->etat_bombe = INACTIVE; /*on desactive la bombe*/
						bombes_actives[j] = NULL;				  /*on supprime le pointeur du tableau des bombes actives*/
						nbr_ennemis = 0;						  /*on recalcule le nombre d'ennemis apres une explotion*/
						for (k = 0; k < MAXENNEMIS; k++)
						{
							if (les_ennemis[k].pv != 0)
							{
								nbr_ennemis++;
							}
						}
					}
					else
					{
						if (t >= 2000)
						{
							afficher_bombe(3, bombes_actives[j]);
						}
						else
						{
							if (t >= 1000)
							{
								afficher_bombe(2, bombes_actives[j]);
							}
						}
					}
				}
			}

			/*On avance un ennemis toutes les 2s = 2000ms*/
			if (tempsActuel - tempsBougerEnnemis >= 2000)
			{ /*si il c'est ecoule au moins 2s depuis que l'on a bouge les ennemis*/
				do
				{
					k = rand() % MAXENNEMIS;
				} while (les_ennemis[k].pv == 0); /*jusqu'a ce que l'on avance un ennemi pas mort*/
				avancer_ennemi(&les_ennemis[k], map);
				tempsBougerEnnemis = tempsActuel;
			}

			/*on recupere le temps et on met a jour nos informations, avant de refaire un tour de boucle  */
			tempsActuel = MLV_get_time() - tempsDepart;
			actualiser_paneau_statistiques(&J1, nbr_ennemis, tempsActuel);
		}

		/*__Fin du jeu___*/
		tempsFinal += tempsActuel;
		if (quitter == 1)
		{
			rejouer = 0; /*on quitte la fonction*/
		}
		else
		{
			/*On affiche la fin du jeu avec le fonction menu_fin_jeu du module menus_jeu_solo*/
			if (nbr_ennemis == 0)
			{
				rejouer = menu_fin_jeu(1, tempsFinal);
			}
			else
			{
				if (J1.pv == 0)
				{
					rejouer = menu_fin_jeu(-1, tempsFinal);
				}
				else
				{
					rejouer = menu_fin_jeu(0, tempsFinal); /*manque de temps*/
				}
			}
		}
	} while (rejouer == 1);

	/*On libere la memoire dedies aux images, animations, lecteurs*/
	release_memory();
}
