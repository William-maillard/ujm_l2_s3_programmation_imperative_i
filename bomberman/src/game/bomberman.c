#include <stdio.h>
#include <MLV/MLV_all.h>

#include "../../inc/game/jeu_solo.h"
#include "../../inc/game/sauvegarde.h"
#include "../../inc/game/meilleurs_scores.h"

void afficher_regles(MLV_Font *font)
{
  MLV_draw_adapted_text_box_with_font(150, 250,
                                      "regles du jeu:\n Pour gagner, frayez-vous vous un chemin jusqu'a vos ennemis\n en explosant tous les obstacles qui entravent votre passage.\nExplosez tous vos ennemis avant le temps impartis (5min)\n et avoir une chance de graver votre nom sur le tableau des meilleurs scores.\nMais prenez garde a vos ennemis,\n ils ne se comportent pas tous de la meme facon....\nCommandes:\nDeplacer le personnages a l'aide des fleches\n de votre clavier.\nAppuyez sur la touche espace pour poser une bombe.\n",
                                      font, 1, MLV_COLOR_BLUE, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_CENTER);

  MLV_actualise_window();
  MLV_wait_seconds(5);
}

int main()
{
  int width = 1000, height = 1000;
  int menuWidth, menuHeight;
  int x, y; /*variables qui vont stocker les coordonnees du click de la souris*/
  MLV_Image *menu;
  MLV_Font *font;

  MLV_create_window("Menu Bomberman", "[BOMBERMAN]", width, height);
  printf("Load menu\n");
  menu = MLV_load_image("./bomberman/medias/background.png");
  /*On redimensionne l'image en gardant les proportions, et on recupere les nouvelles dimensions de l'image pour adapter le taille de la fenetre*/
  MLV_resize_image_with_proportions(menu, width, height);
  MLV_get_image_size(menu, &menuWidth, &menuHeight);
  MLV_change_window_size(menuWidth, menuHeight);

  font = MLV_load_font("./bomberman/medias/FreshLychee.ttf", 30);

  do
  {
    MLV_draw_image(menu, 0, 0);
    MLV_draw_text_with_font(
        425, 350,
        "Nouvelle partie",
        font,
        MLV_COLOR_RED);

    MLV_draw_text_with_font(
        430, 400,
        "Charge partie",
        font,
        MLV_COLOR_GREEN);
    MLV_draw_text_with_font(
        435, 450,
        "multi joueur",
        font,
        MLV_COLOR_BLUE);

    MLV_draw_text_with_font(
        438, 500,
        "meileurs scores",
        font,
        MLV_COLOR_BROWN);

    MLV_draw_text_with_font(
        468, 550,
        "Quitter",
        font,
        MLV_COLOR_BLACK);

    MLV_actualise_window();

    MLV_wait_mouse(&x, &y);
    if ((x >= 425 && x <= 586) && (y >= 350 && y <= 377))
    { /*On apuie sur nouvelle partie"*/
      MLV_clear_window(MLV_COLOR_BLACK);
      MLV_change_window_caption("Jeu -solo- Bomberman", "[BOMBERMAN]");
      jeu_solo("\0");
      MLV_change_window_size(menuWidth, menuHeight); /*on reajuste la fenetre a la taille de l'image du menu*/
    }

    else if ((x >= 430 && x <= 572) && (y >= 400 && y <= 427))
    { /*On apuie sur charge partie*/
      MLV_clear_window(MLV_COLOR_BLACK);
      MLV_change_window_caption("Jeu -solo- Bomberman", "[BOMBERMAN]");
      jeu_solo("./bomberman/save/partieBomberman.txt");
      MLV_change_window_size(menuWidth, menuHeight); /*on reajuste la fenetre a la taille de l'image du menu*/
    }

    else if ((x >= 435 && x <= 569) && (y >= 450 && y <= 477))
    { /*On appuie sur multi joueur*/
      MLV_change_window_caption("Jeu -multi- Bomberman", "[BOMBERMAN]");
      MLV_clear_window(MLV_COLOR_GREEN);
      MLV_actualise_window();
    }

    else if ((x >= 438 && x <= 582) && (y >= 500 && y <= 524))
    { /*On appuie sur meilleurs score*/
      MLV_change_window_caption("Meilleurs scores Bomberman", "[BOMBERMAN]");
      afficher_meilleurs_scores();
      MLV_wait_seconds(5);
      MLV_actualise_window();
    }

    else if ((x >= 34 && x <= 894) && (y >= 42 && y <= 251))
    { /*on appuie sur le logo bomberman*/
      MLV_change_window_caption("Regles du jeu", "[BOMBERMAN]");
      afficher_regles(font);
    }

    MLV_change_window_caption("Menu Bomberman", "[BOMBERMAN]");
  } while (!((x >= 468 && x <= 551) && (y >= 550 && y <= 584))); /*Tant que l'on ne clic pas sur quitter*/

  MLV_free_font(font);
  MLV_free_image(menu);
  MLV_free_window();
  exit(0);
}