/*
Fichier contenant les fonctions permettant d'enregistre et d'afficher les meilleurs scores.
Un score est le temps de fin en millisecondes.
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<MLV/MLV_all.h>
#define N 21 /*nombre de lettres max du nom*/

void enregistrer_meilleur_score(int tempsFinal){
  FILE *top10=NULL; /*va contenire le pointeur vers le fichier des meilleurs scores */
  char nom[21]; /*pour stocker les noms lu, et recuperer le nom du joueur qui a fait un meilleur score*/
  char *nomEntre;
  int score; /*pour stocker le score du top lu*/
  int i=0, j; /*indices qui va servir a stocker le numero de la ligne*/
  int  trouve=0;/*passe a 1 si on trouve un score tel que tempsFinal lui est supperieur*/

  /*1.On ouvre le fichier des sccores en mode lecture et ecriture*/
  if( (top10=fopen("meilleurs_scores.txt", "r+")) == NULL){
    MLV_draw_adapted_text_box(325, 0, "impossible d'ouvrir le fichier des\nmeilleurs scores.", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT);
    MLV_actualise_window();
  }
  else{
    /*On parcours les 10 premieres lignes de notre fichier*/
    while(i < 10 && trouve == 0){
     
      /*A -t- on fait un meilleur score ?*/
      if( (fscanf(top10, "%d", &score) != 1 && fgets(nom, N, top10) == NULL) || tempsFinal < score){/*si la i� ligne est vide ou si on a un temps plus petit que le score lu*/
	trouve= 1;
	rewind(top10);/*on retourne au debut du fichier*/
	for(j=0; j<i;j++){
	  if(fscanf(top10, "%d", &score) != 1 && fgets(nom, N, top10) == NULL){
	    printf("Probleme de lecture de fichier meilleurs-scores.txt\n");
	  }
	}
	/*On est au debut de la i� ligne, et on demande au joueur son nom*/
	MLV_wait_input_box(325, 36, 625, 136, MLV_COLOR_RED, MLV_COLOR_WHITE, MLV_COLOR_BLACK, "Vous avez fait un meilleur score!Veuillez rentrer vortre nom.(20 lettres max)", &nomEntre);
	/*on enregistre le nom et score a la i� ligne en mettant un saut de ligne a la fin*/
	fprintf(top10, "%d, %s\n", tempsFinal, nomEntre);
	
	MLV_draw_adapted_text_box(325, 136, "Score sauvegarde. Merci d'avoir joue.", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT);
      }      
      i++;
    }

    if(trouve == 0){
      MLV_draw_adapted_text_box(325, 0, "Vous ne vous etes pas classe dans le top10\nMerci d'avoir joue.", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT);
    }
  fclose(top10); /*On a fini, on ferme le fichier, si l'ouverture a reussi*/
  }
  MLV_actualise_window(); 
}


void afficher_meilleurs_scores(){
  FILE *top10;/*va contenire le pointeur vers le fichier des meilleurs scores */
  char nom[21];/*pour stocker les noms lu, et recuperer le nom du joueur qui a fait un meilleur score*/
  int score; /*pour stocker le score du top lu*/
  int i; /*indice des lignes du fichier*/
  int Wbox, Hbox; /*pour stockerr la largeur et la longueur des boites de texte*/
  int x=325, y=0; /*pour stocker les coordonnees Nord-Ouest des boites de texte*/
  
  
 /*1.On ouvre le fichier des sccores en mode lecture*/
  if( (top10=fopen("meilleurs_scores.txt", "r")) == NULL){
    MLV_draw_adapted_text_box(325, 0, "impossible d'ouvrir le fichier des\nmeilleurs scores.", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT);
    MLV_actualise_window();
    MLV_wait_seconds(2);
  }
  else{
    /*On affiche les meilleurs scores avec des boites de texte, on recupere les dimensions de la box pour avoir la coordonnee y de la box suivante*/
    MLV_draw_adapted_text_box(x, y, "Meilleurs scores:", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT);
    MLV_get_size_of_adapted_text_box("Meilleurs scores:", 1, &Wbox, &Hbox);
    y+= Hbox;
    
    for(i=0; i<10; i++){
      if(fscanf(top10, "%d", &score) != 1 || fgets(nom, N, top10) == NULL){
	printf("Probleme de lecture de fichier meilleurs-scores.txt, ligne %d %s\n", i, nom);
      }
      else{
	MLV_draw_adapted_text_box(x, y, "%d) %s\t%dminutes %d secondes %d millisecondes", 1, MLV_COLOR_BLACK, MLV_COLOR_WHITE, MLV_COLOR_BLACK, MLV_TEXT_LEFT, i+1, nom, (score/1000)/60, (score/1000)%60, score%1000);
	MLV_get_size_of_adapted_text_box( "%d) %s\t%dminutes %d secondes %d millisecondes", 1, &Wbox, &Hbox, i+1, nom, (score/1000)/60, (score/1000)%60, score%1000);
	y+= Hbox;
      }
    }   
  fclose(top10);/*On ferme le fichier*/ 
  }
  MLV_actualise_window();
}
