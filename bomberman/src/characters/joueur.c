/*
  Fichier contenant toutes les fonctions concernant le joueur et ses bombes; et les boosters.
*/
#include <MLV/MLV_all.h>

#include "../../inc/characters/types.h"
#include "../../inc/graphics/affichage.h"
#include "../../inc/characters/ennemi.h"
#include "../../inc/graphics/images-animations.h" /*on a besoin de caseW et caseH*/

/* TODO moove this declarationi into an header file*/
#define MAX MAXPORTEE * 4 + 1 /*pour faire un tableau contenant les coordonnees des cases ou il faut effacer les flammes apres une explotion*/

/*___Fonction concernant les booster___*/
void ramasser_booster(joueur *J1, map *map, int x, int y)
{
  int i;

  switch (map->pos[x][y].booster)
  {
  case VITESSE:
    /*On augmente la vitesse du perso (voir commentaire de la fonction afficher_avancer_joueur dans le module affichage.c (L160)
     le champs vitesse prends les valeurs 1, 2, 3, 6, et aucune autre */
    switch (J1->vitesse)
    {
    case 1:
      J1->vitesse = 2;
      break;
    case 2:
      J1->vitesse = 3;
      break;
    case 3:
      J1->vitesse = 6;
      break;
    default:
      /*on est a la vitesse maximale*/
      break;
    }
    break;

  case POISON:
    /*ici on ralentit le joueur*/
    switch (J1->vitesse)
    {
    case 2:
      J1->vitesse = 1;
      break;
    case 3:
      J1->vitesse = 2;
      break;
    case 6:
      J1->vitesse = 3;
      break;
    default:
      /*on est a la vitesse minimale*/
      break;
    }
    break;

  case DEGAT:
    if (J1->bombes[0].degat < 4)
    { /*si les bombes ont pas les degat au max*/
      /*on augmante de 1 les degats de toutes les bombes du joueur*/
      for (i = 0; i < MAXBOMBES; i++)
      {
        J1->bombes[i].degat++;
      }
    }
    break;

  case LIFE:
    if (J1->pv < MAXPVJOUEUR)
    {
      J1->pv++;
    }
    break;

  case PORTEE:
    if (J1->bombes[0].portee < MAXPORTEE)
    {
      for (i = 0; i < MAXBOMBES; i++)
      {
        J1->bombes[i].portee++;
      }
    }
    break;

  case ADD_BOMBE:
    if (J1->nombre_bombes < MAXBOMBES)
    {
      J1->nombre_bombes++;
    }
    break;

  default:
    break;
  }
  /*on met a jour la map, l'etat case est mis a jour par la fonction avancer_joueur*/
  map->pos[x][y].booster = AUCUN;
}

void drop_booster(map *map, int x, int y)
{
  char pioche[16]; /*tableau dans lequel on va piocher*/
  int boost;       /*indice du boos qu'on pioche*/

  /*On remplit le tableau avec les proportions suivantes:
    -5 AUCUN
    -2 VITESSE
    -1 POISON
    -2 DEGAT
    -2 LIFE
    -2 PORTEE
    -2 ADD_BOMBE
*/
  pioche[0] = AUCUN;
  pioche[1] = VITESSE;
  pioche[2] = POISON;
  pioche[3] = DEGAT;
  pioche[4] = AUCUN;
  pioche[5] = LIFE;
  pioche[6] = PORTEE;
  pioche[7] = AUCUN;
  pioche[8] = ADD_BOMBE;
  pioche[9] = VITESSE;
  pioche[10] = PORTEE;
  pioche[11] = AUCUN;
  pioche[12] = LIFE;
  pioche[13] = DEGAT;
  pioche[14] = AUCUN;
  pioche[15] = ADD_BOMBE;

  /*on choisi au hazard un indice*/
  boost = rand() % 16; /*rand est initialise dans la fonction jeu_solo*/
  /*Si ce n'est pas un AUCUN ont le place sur la map et l'affiche*/
  if (pioche[boost] != AUCUN)
  {
    map->pos[x][y].etat_case = BOOSTER;
    map->pos[x][y].booster = pioche[boost];
    afficher_obstacle(x, y);
  }
}

/*___Fonction pour le joueur___*/

void joueur_perd_pv(joueur *J1, bombe *bombe, map *map)
{
  int x = J1->coordonnees.x, y = J1->coordonnees.y;
  int mort = 0;
  /*
    1.On test si les degats sont fait par une bombe sinon c'est par un ennemi,
    2.Si pv-degat<=0 alors le joueur est mort. On affiche la mort du joueur,
    3.Sinon on enleve les pv au joueur.
    4.S'il est mort on actualise la case de la map.
  */
  if (bombe != NULL)
  {
    if ((J1->pv - bombe->degat) <= 0)
    {
      J1->pv = 0;
      mort = 1;
    }
    else
    {
      J1->pv -= bombe->degat;
    }
  }
  else
  { /*les ennemis font 1 de degat*/
    J1->pv -= 1;
    if (J1->pv == 0)
    {
      mort = 1;
    }
  }
  if (mort == 1)
  {
    afficher_mort_joueur(J1);
    if (map->pos[x][y].etat_case == ENNEMI_JOUEUR)
    {
      map->pos[x][y].etat_case = ENNEMI;
    }
    else
    {
      map->pos[x][y].etat_case = VIDE;
    }
    map->pos[x][y].joueur = NULL;
  }
}

int avancer_joueur(sens sens, map *map, joueur *J1)
{
  int x, y, mouvement = -1;
  x = J1->coordonnees.x;
  y = J1->coordonnees.y;
  /*On switch le sens du deplacement:
    1.On stocke le sens de deplacement dans la variable mouvement(si pas de mouvement la valeur sera de -1)
    2.si la case d'arrive le permet, On affiche le deplacement et interagit avec ce qu'il y a dans la case d'arrivee,
    2.On met a jour la case d'arrivee.
    3.Sinon, on affiche le joueur dans la position qui correspond au sens et mouvement prend la valeur -1.
    4.Si mouvement != -1, on met a jour la case de depart car il y a eu un deplacement
      s'il y a un ennemi on le reaffiche car le deplacement du joueur l'a efface.
    5.On renvoie mouvement.
  */

  switch (sens)
  {
  case DOWN:
    mouvement = DOWN;
    switch (map->pos[x][y + 1].etat_case)
    {
    case VIDE:
      afficher_avancer_joueur(sens, J1);
      J1->coordonnees.y++;
      map->pos[x][y + 1].etat_case = JOUEUR;
      map->pos[x][y + 1].joueur = J1;
      break;

    case BOOSTER:
      afficher_avancer_joueur(sens, J1);
      ramasser_booster(J1, map, x, y + 1);
      J1->coordonnees.y++;
      map->pos[x][y + 1].etat_case = JOUEUR;
      map->pos[x][y + 1].joueur = J1;
      break;

    case ENNEMI:
      afficher_avancer_joueur(sens, J1);
      J1->coordonnees.y++;
      afficher_ennemi_joueur(J1, map->pos[x][y + 1].ennemi);
      map->pos[x][y + 1].etat_case = ENNEMI_JOUEUR;
      map->pos[x][y + 1].joueur = J1;
      joueur_perd_pv(J1, NULL, map);
      break;

    default:
      afficher_joueur(x, y);
      mouvement = -1;
      break;
    }
    break;

  case UP:
    mouvement = UP;
    switch (map->pos[x][y - 1].etat_case)
    {
    case VIDE:
      afficher_avancer_joueur(sens, J1);
      J1->coordonnees.y--;
      map->pos[x][y - 1].etat_case = JOUEUR;
      map->pos[x][y - 1].joueur = J1;
      break;

    case BOOSTER:
      afficher_avancer_joueur(sens, J1);
      ramasser_booster(J1, map, x, y - 1);
      J1->coordonnees.y--;
      map->pos[x][y - 1].etat_case = JOUEUR;
      map->pos[x][y - 1].joueur = J1;
      break;

    case ENNEMI:
      afficher_avancer_joueur(sens, J1);
      J1->coordonnees.y--;
      afficher_ennemi_joueur(J1, map->pos[x][y - 1].ennemi);
      map->pos[x][y - 1].etat_case = ENNEMI_JOUEUR;
      map->pos[x][y - 1].joueur = J1;
      joueur_perd_pv(J1, NULL, map);
      break;

    default:
      afficher_joueur_up(x, y);
      mouvement = -1;
      break;
    }
    break;

  case LEFT:
    mouvement = LEFT;
    switch (map->pos[x - 1][y].etat_case)
    {
    case VIDE:
      afficher_avancer_joueur(sens, J1);
      J1->coordonnees.x--;
      map->pos[x - 1][y].etat_case = JOUEUR;
      map->pos[x - 1][y].joueur = J1;
      break;

    case BOOSTER:
      afficher_avancer_joueur(sens, J1);
      ramasser_booster(J1, map, x - 1, y);
      J1->coordonnees.x--;
      map->pos[x - 1][y].etat_case = JOUEUR;
      map->pos[x - 1][y].joueur = J1;
      break;

    case ENNEMI:
      afficher_avancer_joueur(sens, J1);
      J1->coordonnees.x--;
      afficher_ennemi_joueur(J1, map->pos[x - 1][y].ennemi);
      map->pos[x - 1][y].etat_case = ENNEMI_JOUEUR;
      map->pos[x - 1][y].joueur = J1;
      joueur_perd_pv(J1, NULL, map);
      break;

    default:
      afficher_joueur_left(x, y);
      mouvement = -1;
      break;
    }
    break;

  case RIGHT:
    mouvement = RIGHT;
    switch (map->pos[x + 1][y].etat_case)
    {
    case VIDE:
      afficher_avancer_joueur(sens, J1);
      J1->coordonnees.x++;
      map->pos[x + 1][y].etat_case = JOUEUR;
      map->pos[x + 1][y].joueur = J1;
      break;

    case BOOSTER:
      afficher_avancer_joueur(sens, J1);
      ramasser_booster(J1, map, x + 1, y);
      J1->coordonnees.x++;
      map->pos[x + 1][y].etat_case = JOUEUR;
      map->pos[x + 1][y].joueur = J1;
      break;

    case ENNEMI:
      afficher_avancer_joueur(sens, J1);
      J1->coordonnees.x++;
      afficher_ennemi_joueur(J1, map->pos[x + 1][y].ennemi);
      map->pos[x + 1][y].etat_case = ENNEMI_JOUEUR;
      map->pos[x + 1][y].joueur = J1;
      joueur_perd_pv(J1, NULL, map);
      break;

    default:
      afficher_joueur_right(x, y);
      mouvement = -1;
      break;
    }
    break;
  }
  if (mouvement != -1)
  { /*si le joueur a avancer*/
    map->pos[x][y].joueur = NULL;
    if (map->pos[x][y].etat_case == ENNEMI_JOUEUR)
    {
      map->pos[x][y].etat_case = ENNEMI;
      afficher_ennemi(map->pos[x][y].ennemi->sens, x, y);
    }
    else
    {
      map->pos[x][y].etat_case = VIDE;
    }
  }
  return mouvement;
}

/*___Fonctions concernant la bombe___*/

bombe *poser_bombe(joueur *J1, map *map)
{
  bombe *bombe = NULL;
  int x = J1->coordonnees.x, y = J1->coordonnees.y, sens = 0, i = 0;
  /*
    1.On verifie si le joueur peut poser une bombe,
    2.Si oui, alors on prends une bombe du joueur,
    3.On change les coordonnees, on fait automatiquement bouger le joueur(le 1� sens ou il peut se deplacer dans l'ordre suivant: U,D,L,R), puis on affiche la bombe
    4.On active le timer de la bombe, et on change son etat
    5.On met a jour la case de la map,
    6.On renvoie un pointeur sur la bombe activee,
    6.Si (1.)faux alors on renvoie NULL.
  */

  while (i < J1->nombre_bombes)
  {
    /*1*/
    if (J1->bombes[i].etat_bombe == INACTIVE && map->pos[x][y].etat_case == JOUEUR)
    { /*Si on trouve une bombe inactive (et on ne pose pas de bombes sur les ennemis)*/
      /*2*/
      bombe = &J1->bombes[i];
      /*3*/
      bombe->coordonnees.x = x;
      bombe->coordonnees.y = y;
      while (avancer_joueur(sens, map, J1) == -1 && sens < 4)
      {
        sens++;
      }
      afficher_bombe(1, bombe);
      /*4*/
      bombe->timer = MLV_get_time();
      bombe->etat_bombe = ACTIVE;
      /*5*/
      map->pos[x][y].etat_case = BOMBE;
      map->pos[x][y].bombe = bombe;
    }
    i++;
  }
  return bombe;
}

void detruire_obstacle(map *map, int x, int y)
{
  if (map->pos[x][y].obstacle == MUR)
  { /*On verifie que l'obstacle peut etre detruit*/
    afficher_destruction_mur(x, y);
    map->pos[x][y].etat_case = VIDE;
    drop_booster(map, x, y);
  }
}

void exploser_bombe(bombe *bombe, map *map)
{
  coordonnees cases_a_effacer[MAX];                                              /*stocke les coordonnees des cases affectees par l'explotion, pour les reaficher apres l'explotion*/
  int portee = bombe->portee, i = 1, j = 0;                                      /*i indice de map et j indice de cases_a_effacer*/
  int affichageup = 0, affichagedown = 0, affichageleft = 0, affichageright = 0; /*si !=0 alors on n'affiche pas de flammes (utile pour ne pas afficher de flammes sur nos mur/delimiteurs)*/
  int up = 0, down = 0, right = 0, left = 0;                                     /*passe a 1 si l'explotion rencontre un obstacle/joueur/ennemi/bombe*/
  int x = bombe->coordonnees.x, y = bombe->coordonnees.y;

  MLV_change_frame_rate(1); /*On va afficher l'explosion pendant 1s*/
                            /*
                              1.On met a jour la case ou se trouve la bombe,
                              2.On affiche le centre de l'explotion,
                              3.Tant que la portee est supperieur ou egale a 0 on test l'etat de la case a i atour du centre pour interagir avec ce qu'il y a dessus,
                              4.On affiche la partie du souffle sur la case si porte>0, sinon on affiche la fin du souffle de l'explosion
                              5.On se sert de notre tableau cases_a_effacer pour effacer les flammes, et afficher la case selon son etat_case.
                              (6.Remarque: l'etat de la bombe est actualise dans jeu_solo)
                            */

  /*1*/ map->pos[x][y].etat_case = VIDE;
  map->pos[x][y].bombe = NULL;
  /*2*/ afficher_centre_explosion(bombe);
  /*3*/ while (portee >= 0)
  {
    /*UP*/
    if (up == 0)
    {
      switch (map->pos[x][y - i].etat_case)
      {
      case OBSTACLE:
        detruire_obstacle(map, x, y - i);
        affichageup = -1;
        up = 1;
        break;
      case JOUEUR:
        joueur_perd_pv(map->pos[x][y - i].joueur, bombe, map);
        up = 1;
        break;
      case BOMBE:
        up = 1;
        break;
      case ENNEMI:
        ennemi_perd_pv(map->pos[x][y - i].ennemi, bombe, map);
        up = 1;
        break;
      case BOOSTER:
        map->pos[x][y - i].etat_case = VIDE;
        map->pos[x][y - i].booster = AUCUN;
        break;
      case ENNEMI_JOUEUR:
        joueur_perd_pv(map->pos[x][y - i].joueur, bombe, map);
        ennemi_perd_pv(map->pos[x][y - i].ennemi, bombe, map);
        up = 1;
        break;
      case ENNEMI_BOOSTER:
        map->pos[x][y - i].etat_case = ENNEMI;
        map->pos[x][y - i].booster = AUCUN;
        ennemi_perd_pv(map->pos[x][y - i].ennemi, bombe, map);
        up = 1;
        break;
      case VIDE:
        break;
      }
      /*4*/ if (portee > 0 && affichageup == 0)
      {
        afficher_partie_explosion(UP, bombe->degat, x, y - i);
      }
      if (portee == 0 && affichageup == 0)
      {
        afficher_fin_explosion(UP, bombe->degat, x, y - i);
      }
      cases_a_effacer[j].x = x;
      cases_a_effacer[j].y = y - i;
      j++;
    }
    /*DOWN*/
    if (down == 0)
    {
      switch (map->pos[x][y + i].etat_case)
      {
      case OBSTACLE:
        detruire_obstacle(map, x, y + i);
        affichagedown = -1;
        down = 1;
        break;
      case JOUEUR:
        joueur_perd_pv(map->pos[x][y + i].joueur, bombe, map);
        down = 1;
        break;
      case BOMBE:
        down = 1;
        break;
      case ENNEMI:
        ennemi_perd_pv(map->pos[x][y + i].ennemi, bombe, map);
        down = 1;
        break;
      case BOOSTER:
        map->pos[x][y + i].etat_case = VIDE;
        map->pos[x][y + i].booster = AUCUN;
        break;
      case ENNEMI_JOUEUR:
        joueur_perd_pv(map->pos[x][y + i].joueur, bombe, map);
        ennemi_perd_pv(map->pos[x][y + i].ennemi, bombe, map);
        down = 1;
        break;
      case ENNEMI_BOOSTER:
        map->pos[x][y + i].etat_case = ENNEMI;
        map->pos[x][y + i].booster = AUCUN;
        ennemi_perd_pv(map->pos[x][y + i].ennemi, bombe, map);
        down = 1;
        break;
      case VIDE:
        break;
      }
      if (portee > 0 && affichagedown == 0)
      {
        afficher_partie_explosion(DOWN, bombe->degat, x, y + i);
      }
      if (portee == 0 && affichagedown == 0)
      {
        afficher_fin_explosion(DOWN, bombe->degat, x, y + i);
      }
      cases_a_effacer[j].x = x;
      cases_a_effacer[j].y = y + i;
      j++;
    }
    /*LEFT*/
    if (left == 0)
    {
      switch (map->pos[x - i][y].etat_case)
      {
      case OBSTACLE:
        detruire_obstacle(map, x - i, y);
        affichageleft = -1;
        left = 1;
        break;
      case JOUEUR:
        joueur_perd_pv(map->pos[x - i][y].joueur, bombe, map);
        left = 1;
        break;
      case BOMBE:
        left = 1;
        break;
      case ENNEMI:
        ennemi_perd_pv(map->pos[x - i][y].ennemi, bombe, map);
        left = 1;
        break;
      case BOOSTER:
        map->pos[x - i][y].etat_case = VIDE;
        map->pos[x - i][y].booster = AUCUN;
        break;
      case ENNEMI_JOUEUR:
        joueur_perd_pv(map->pos[x - i][y].joueur, bombe, map);
        ennemi_perd_pv(map->pos[x - i][y].ennemi, bombe, map);
        left = 1;
        break;
      case ENNEMI_BOOSTER:
        map->pos[x - i][y].etat_case = ENNEMI;
        map->pos[x - i][y].booster = AUCUN;
        ennemi_perd_pv(map->pos[x - i][y].ennemi, bombe, map);
        left = 1;
        break;
      case VIDE:
        break;
      }
      if (portee > 0 && affichageleft == 0)
      {
        afficher_partie_explosion(LEFT, bombe->degat, x - i, y);
      }
      if (portee == 0 && affichageleft == 0)
      {
        afficher_fin_explosion(LEFT, bombe->degat, x - i, y);
      }
      cases_a_effacer[j].x = x - i;
      cases_a_effacer[j].y = y;
      j++;
    }
    /*RIGHT*/
    if (right == 0)
    {
      switch (map->pos[x + i][y].etat_case)
      {
      case OBSTACLE:
        detruire_obstacle(map, x + i, y);
        affichageright = -1;
        right = 1;
        break;
      case JOUEUR:
        joueur_perd_pv(map->pos[x + i][y].joueur, bombe, map);
        right = 1;
        break;
      case BOMBE:
        right = 1;
        break;
      case ENNEMI:
        ennemi_perd_pv(map->pos[x + i][y].ennemi, bombe, map);
        right = 1;
        break;
      case BOOSTER:
        map->pos[x + i][y].etat_case = VIDE;
        map->pos[x + i][y].booster = AUCUN;
        break;
      case ENNEMI_JOUEUR:
        joueur_perd_pv(map->pos[x + i][y].joueur, bombe, map);
        ennemi_perd_pv(map->pos[x + i][y].ennemi, bombe, map);
        right = 1;
        break;
      case ENNEMI_BOOSTER:
        map->pos[x + i][y].etat_case = ENNEMI;
        map->pos[x + i][y].booster = AUCUN;
        ennemi_perd_pv(map->pos[x + i][y].ennemi, bombe, map);
        right = 1;
        break;
      case VIDE:
        break;
      }
      if (portee > 0 && affichageright == 0)
      {
        afficher_partie_explosion(RIGHT, bombe->degat, x + i, y);
      }
      if (portee == 0 && affichageright == 0)
      {
        afficher_fin_explosion(RIGHT, bombe->degat, x + i, y);
      }
      cases_a_effacer[j].x = x + i;
      cases_a_effacer[j].y = y;
      j++;
    }
    portee--;
    i++;
  }
  /*On affiche les changements pendant 1 secondes avant d'effacer l'explotion*/
  MLV_actualise_window();
  MLV_delay_according_to_frame_rate();
  /*-Fin boucle while, on passe a l'etape 5 -*/
  if (j < MAX)
  {
    cases_a_effacer[j].x = 0;
    cases_a_effacer[j].y = 0; /*pour savoir quand on doit s'arreter dans le tableau*/
  }
  j = 0;
  while (!(x == 0 && y == 0) && (j < MAX))
  { /*le premier tour de boucle correspond au centre de l'explostion*/
    switch (map->pos[x][y].etat_case)
    {
    case JOUEUR:
      afficher_joueur(x, y);
      break;
    case BOMBE:
      afficher_bombe(3, map->pos[x][y].bombe);
      break;
    case ENNEMI:
      afficher_ennemi_surpris(map->pos[x][y].ennemi);
      break;
    case OBSTACLE:
      if (map->pos[x][y].obstacle == MUR)
      {
        effacer(caseW * x, caseH * y);
      }
      break;
    case ENNEMI_JOUEUR:
      afficher_ennemi_joueur(map->pos[x][y].joueur, map->pos[x][y].ennemi);
      break;
    case BOOSTER:
      afficher_booster(map->pos[x][y].booster, x, y);
      break;
    default:
      effacer(caseW * x, caseH * y);
      break;
    }
    x = cases_a_effacer[j].x;
    y = cases_a_effacer[j].y;
    j++;
  }
}
