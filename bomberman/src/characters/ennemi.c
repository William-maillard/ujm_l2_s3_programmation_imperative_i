/*
 * Fichier contenant toutes les fonctions concernant les actions de l'ennemi.
 */
#include <MLV/MLV_all.h>
#include "../../inc/characters/types.h"
#include "../../inc/graphics/affichage.h"
#include "../../inc/characters/joueur.h"

/*__________fonction permettant de faire avancer l'ennemi__________*/

void avancer_ennemi(ennemi *ennemi, map *map)
{
  int x = ennemi->coordonnees.x, y = ennemi->coordonnees.y, mouvement = -1;

  /*1.On cherche dans quel sens se deplace l'ennemi et mouvement prend le sens du deplacement
    2.S'il y a un obstacle/ennemi/bombe, l'ennemi prend le sens oppose au sien(default), et mouvement prend la valeur -1.
    3.Sinon il monte d'une case:
    4.On affiche le deplacement.
    5.On met a jour les coordonnnees de l'ennemi
    6.On met a jour la case d'arrivee.
    7.Si mouvement != -1 on met a jour la case de depart
    On fait ceci pour chaque direction.
  */

  switch (ennemi->sens)
  {

  case UP:
    mouvement = UP;
    switch (map->pos[x][y - 1].etat_case)
    {

    case VIDE:
      afficher_avancer_ennemi(ennemi);
      ennemi->coordonnees.y--;
      map->pos[x][y - 1].ennemi = ennemi;
      map->pos[x][y - 1].etat_case = ENNEMI;
      break;

    case JOUEUR:
      afficher_avancer_ennemi(ennemi);
      afficher_ennemi_joueur(map->pos[x][y - 1].joueur, ennemi);
      ennemi->coordonnees.y--;
      map->pos[x][y - 1].ennemi = ennemi;
      map->pos[x][y - 1].etat_case = ENNEMI_JOUEUR;
      break;

    case BOOSTER:
      afficher_avancer_ennemi(ennemi);
      ennemi->coordonnees.y--;
      map->pos[x][y - 1].ennemi = ennemi;
      map->pos[x][y - 1].etat_case = ENNEMI_BOOSTER;
      break;

    default:
      mouvement = -1;
      ennemi->sens = DOWN;
      break;
    }
    break;

  case DOWN:
    mouvement = DOWN;
    switch (map->pos[x][y + 1].etat_case)
    {

    case VIDE:
      afficher_avancer_ennemi(ennemi);
      ennemi->coordonnees.y++;
      map->pos[x][y + 1].ennemi = ennemi;
      map->pos[x][y + 1].etat_case = ENNEMI;
      break;

    case JOUEUR:
      afficher_avancer_ennemi(ennemi);
      afficher_ennemi_joueur(map->pos[x][y + 1].joueur, ennemi);
      ennemi->coordonnees.y++;
      map->pos[x][y + 1].ennemi = ennemi;
      map->pos[x][y + 1].etat_case = ENNEMI_JOUEUR;
      break;

    case BOOSTER:
      afficher_avancer_ennemi(ennemi);
      ennemi->coordonnees.y++;
      map->pos[x][y + 1].ennemi = ennemi;
      map->pos[x][y + 1].etat_case = ENNEMI_BOOSTER;
      break;

    default:
      mouvement = -1;
      ennemi->sens = UP;
      break;
    }
    break;

  case LEFT:
    mouvement = LEFT;
    switch (map->pos[x - 1][y].etat_case)
    {

    case VIDE:
      afficher_avancer_ennemi(ennemi);
      ennemi->coordonnees.x--;
      map->pos[x - 1][y].ennemi = ennemi;
      map->pos[x - 1][y].etat_case = ENNEMI;
      break;

    case JOUEUR:
      afficher_avancer_ennemi(ennemi);
      afficher_ennemi_joueur(map->pos[x - 1][y].joueur, ennemi);
      ennemi->coordonnees.x--;
      map->pos[x - 1][y].ennemi = ennemi;
      map->pos[x - 1][y].etat_case = ENNEMI_JOUEUR;
      break;

    case BOOSTER:
      afficher_avancer_ennemi(ennemi);
      ennemi->coordonnees.x--;
      map->pos[x - 1][y].ennemi = ennemi;
      map->pos[x - 1][y].etat_case = ENNEMI_BOOSTER;
      break;

    default:
      mouvement = -1;
      ennemi->sens = RIGHT;
      break;
    }
    break;

  case RIGHT:
    mouvement = RIGHT;
    switch (map->pos[x + 1][y].etat_case)
    {

    case VIDE:
      afficher_avancer_ennemi(ennemi);
      ennemi->coordonnees.x++;
      map->pos[x + 1][y].ennemi = ennemi;
      map->pos[x + 1][y].etat_case = ENNEMI;
      break;

    case JOUEUR:
      afficher_avancer_ennemi(ennemi);
      afficher_ennemi_joueur(map->pos[x + 1][y].joueur, ennemi);
      ennemi->coordonnees.x++;
      map->pos[x + 1][y].ennemi = ennemi;
      map->pos[x + 1][y].etat_case = ENNEMI_JOUEUR;
      break;

    case BOOSTER:
      afficher_avancer_ennemi(ennemi);
      ennemi->coordonnees.x++;
      map->pos[x + 1][y].ennemi = ennemi;
      map->pos[x + 1][y].etat_case = ENNEMI_BOOSTER;
      break;

    default:
      mouvement = -1;
      ennemi->sens = LEFT;
      break;
    }
    break;
  }
  if (mouvement != -1)
  {
    map->pos[x][y].ennemi = NULL;
    switch (map->pos[x][y].etat_case)
    {
    case ENNEMI:
      map->pos[x][y].etat_case = VIDE;
      break;
    case ENNEMI_JOUEUR:
      map->pos[x][y].etat_case = JOUEUR;
      afficher_joueur(x, y);
      break;
    case ENNEMI_BOOSTER:
      map->pos[x][y].etat_case = BOOSTER;
      afficher_booster(map->pos[x][y].booster, x, y);
      break;
    default:
      break;
    }
  }
}

void ennemi_perd_pv(ennemi *E1, bombe *bombe, map *map)
{
  int x = E1->coordonnees.x, y = E1->coordonnees.y;

  /*1.On test si l'ennemi meurt ou reste en vie.(rappel le champs pv est unsigned)
    2.Si il meurt on met les pv a 0, puis on affiche la mort de l'ennemi, et on met a jour la case.
    3.S'il n'y a pas de joueur sur la case on drop un booster (fct de joueur.c)
    4.Sinon, on lui enleve les pv, et on affiche l'image de l'ennemi surpris.
  */
  if (E1->pv - bombe->degat <= 0)
  {
    E1->pv = 0;
    afficher_mort_ennemi(E1);
    map->pos[x][y].ennemi = NULL;
    if (map->pos[x][y].etat_case == ENNEMI_JOUEUR)
    {
      map->pos[x][y].etat_case = JOUEUR;
    }
    else
    {
      map->pos[x][y].etat_case = VIDE; /*on ne traite pas le cas ENNEMI_BOOSTER car avec l'explsoion �a devient ENNEMI*/
      drop_booster(map, x, y);
    }
  }
  else
  {
    E1->pv -= bombe->degat;
    afficher_ennemi_surpris(E1);
  }
}
